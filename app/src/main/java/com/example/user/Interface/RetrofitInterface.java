package com.example.user.Interface;
import com.example.user.Response.AddFave_Repo;
import com.example.user.Response.All_Skill;
import com.example.user.Response.ConfirmOrder_Repo;
import com.example.user.Response.ConfirmOrder_data;
import com.example.user.Response.Emp_Sch_Repo;
import com.example.user.Response.Fave_Repo;
import com.example.user.Response.Login_res;
import com.example.user.Response.PlaceOrder_Repo;
import com.example.user.Response.RemoveFave_Repo;
import com.example.user.Response.Remove_Shortlist_Repo;
import com.example.user.Response.ShortgetList_Repo;
import com.example.user.Response.ShortlistRepo;
import com.example.user.Response.Shortlistcount_Repo;
import com.example.user.Response.Skill_Repo_fee;
import com.example.user.Response.StatusChange_Repo;
import com.example.user.Response.TagPost_Repo;
import com.example.user.Response.TagsGet_Repo;
import com.example.user.Response.Track_Inp_Repo;
import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface RetrofitInterface {


    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/workers.php")
    Call<Skill_Repo_fee> getStoreData(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/primary_skills.php")
    Call<All_Skill> getAll();


    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/user_login.php")
    Call<Login_res> login(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/shortlist.php")
    Call<ShortlistRepo> postshort(@Body JsonObject jsonObject);


    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/get_shortlist.php")
    Call<ShortgetList_Repo> getshortlist(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/addTag.php")
    Call<TagPost_Repo> posttag(@Body JsonObject jsonObject);


    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/getTagList.php")
    Call<TagsGet_Repo> gettagslist(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/addOrder.php")
    Call<PlaceOrder_Repo> placeorder(@Body JsonObject jsonObject);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/userEmployments.php")
    Call<Emp_Sch_Repo> getshedule(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/userEmployments.php")
    Call<Track_Inp_Repo> getinprogress(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/user_order_list.php")
    Call<Emp_Sch_Repo> getinprogress(@QueryMap Map<String, String> param);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/shortlist.php")
    Call<Remove_Shortlist_Repo> removeshortlist(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/favourite.php")
    Call<AddFave_Repo> addfaverate(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/get_favourites.php")
    Call<Fave_Repo> getfaverate(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/favourite.php")
    Call<RemoveFave_Repo> removefav(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/updateOrderStatus.php")
    Call<StatusChange_Repo>statuschange(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/confirm_order.php")
    Call<ConfirmOrder_Repo> confirmorder(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/getShortListCount.php?user_id=38")
    Call<Shortlistcount_Repo> getshortlistcount(@QueryMap Map<String, String> param);

}

