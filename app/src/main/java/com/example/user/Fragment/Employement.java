package com.example.user.Fragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.Activity.Skills;
import com.example.user.Activity.SuccessFull;
import com.example.user.Activity.TagsActivity;
import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.R;
import com.example.user.Response.AddFave_Repo;
import com.example.user.Response.All_Skill;
import com.example.user.Response.DataList;
import com.example.user.Response.Data_AllSkill;
import com.example.user.Response.PlaceOrder_Repo;
import com.example.user.Response.RemoveFave_Repo;
import com.example.user.Response.ShortlistRepo;
import com.example.user.Response.Skill_Repo_fee;
import com.example.user.Response.TagsGet_Repo;
import com.example.user.Response.TagsGet_data;
import com.example.user.adapter.AllSkill_Adapter;
import com.example.user.adapter.Users_Fee;
import com.example.user.service.RemoteData;
import com.example.user.service.RemoteData_emp;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Employement extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
  AutoCompleteTextView autoCompleteTextView;
  String secondaryskill,userid,selected,wrkid,startdate;
    private Users_Fee usersadapter;
    private AllSkill_Adapter skill_adapter;
    private RecyclerView recyclerView,recyclerViewall;
    List<DataList> dataLists;
    List<Data_AllSkill> allSkills;
SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog progressDialog;
    JsonObject jsonObject;
    Spinner spinner;
    List<TagsGet_data> dataLists1;
    List<String> taglist;
    List<String> tagid;
    ArrayAdapter<String> arrayAdapter;
    Integer nmmm;
    private String date,today;
    SharedPreferenceClass sharedPreferenceClass;
    public Employement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_employement, container, false);
        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userid = sharedPreferenceClass.getValue_string("user_id");
        autoCompleteTextView=view.findViewById(R.id.employment_search);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),"font/Uber Move Text.ttf");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateandTime = sdf.format(new Date());

        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        today = dayFormat.format(new Date());
        nmmm=  sdf.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
        taglist=new ArrayList<>();
        tagid=new ArrayList<>();
        autoCompleteTextView.setHint("Search for a service");
        autoCompleteTextView.setTypeface(font);
        recyclerView=view.findViewById(R.id.recyclerview);
        recyclerViewall=view.findViewById(R.id.recyclerviewall);
        autoCompleteTextView.setOnItemClickListener(onItemClickListener);
        swipeRefreshLayout=view.findViewById(R.id.swipereferesh1);
        progressDialog=new ProgressDialog(getContext());
          getTags();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (secondaryskill.equals(" ")){
                    usersadapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    recyclerViewall.setVisibility(View.VISIBLE);
                    AllSkill();
                }else {
                    recyclerView.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    recyclerViewall.setVisibility(View.GONE);
                    UsersDetails();
                }

            }
        });

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                secondaryskill= autoCompleteTextView.getText().toString();
                Map<String, Object> data = new HashMap<>();
                data.put("keyword",secondaryskill);
                RemoteData_emp remoteData = new RemoteData_emp(getContext());
                remoteData.getdata(data);

            }

            @Override
            public void afterTextChanged(Editable s) {
                    if (secondaryskill.isEmpty()){
//                        usersadapter.notifyDataSetChanged();
                        recyclerView.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        recyclerViewall.setVisibility(View.VISIBLE);
                        AllSkill();

                    }else {
                        recyclerView.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        recyclerViewall.setVisibility(View.GONE);
                        UsersDetails();
                    }

            }
        });
        AllSkill();
        return view;
    }

    private void UsersDetails() {
        recyclerViewall.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        Map<String, String> data = new HashMap<>();
        data.put("skill",secondaryskill);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Skill_Repo_fee> call = jsonpost.getStoreData(data);
        call.enqueue(new Callback<Skill_Repo_fee>() {
            @Override
            public void onResponse(Call<Skill_Repo_fee> call, retrofit2.Response<Skill_Repo_fee> response) {
                if (response.isSuccessful()){
                    swipeRefreshLayout.setRefreshing(false);
                    dataLists= response.body().getData();
                    if (getActivity()!=null){
                        usersadapter=new Users_Fee(getContext(),dataLists);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(usersadapter);

                        usersadapter.setOnITemClickListener(new Users_Fee.OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                wrkid = dataLists.get(position).getWorker_id();
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                                final View dialogView =getLayoutInflater().inflate(R.layout.placeorder_dialog, null);
                                dialog.setView(dialogView);
                                dialog.setCancelable(false);
                                Button dialog_button = (Button) dialogView.findViewById(R.id.addtag);
                                spinner = dialogView.findViewById(R.id.days);
                                spinner.setAdapter(arrayAdapter);
                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        selected = tagid.get(i);
                                        Toast.makeText(getContext(), selected, Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                                Button textView = dialogView.findViewById(R.id.dialog_button);
                                dialog_button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent tagintent = new Intent(getContext(), TagsActivity.class);
                                        startActivity(tagintent);
                                    }
                                });
                                final AlertDialog alertDialog = dialog.create();
                                textView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                       // SharedPreferences sharedPreferences =getSharedPreferences("SharedTag",MODE_PRIVATE);
                                        // selected = sharedPreferences.getString("tag",null);

                                        alertDialog.dismiss();
                                        progressDialog.setMessage("Wait....");
                                        progressDialog.show();
                                        placeOrder(selected,wrkid);
                                       /* if (tagv!=null){
                                        }else {

                                            Toast.makeText(Skills.this, "please add tag", Toast.LENGTH_SHORT).show();
                                        }*/


                                    }
                                });
                                alertDialog.show();
                            }

                            @Override
                            public void onShorlistItemClick(int position) {
                                progressDialog.setMessage("Wait....");
                                progressDialog.show();
                                String workerid = dataLists.get(position).getWorker_id();
                                SharedPreferences sharedPreferences =getContext().getSharedPreferences("WorkerSHARED", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("workerid",workerid);
                                editor.apply();
                                postShortlis(workerid);
                            }

                            @Override
                            public void onAddFaverateClick(int position) {
                                String workerid = dataLists.get(position).getWorker_id();
                                SharedPreferences sharedPreferences =getContext().getSharedPreferences("WorkerSHARED",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("workerid",workerid);
                                editor.apply();
                                addToFaverate(workerid);
                            }

                            @Override
                            public void onRemoveFaverateClick(int position) {
                                String workerid = dataLists.get(position).getWorker_id();
                                removeItem(workerid);
                            }

                            @Override
                            public void onCalnderpicker(int position) {
                                datePicker();
                            }

                            @Override
                            public void onavailable(int position) {
                               availabledate();
                            }

                            @Override
                            public void onrateclick(int position) {

                            }
                        });
                    }else
                    {
                        swipeRefreshLayout.setRefreshing(false);

                    }



                }else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Skill_Repo_fee> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Log.d("error",t.getMessage());
                Toast.makeText(getContext(), "something went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void datePicker() {
        // Get Calendar instance
        final Calendar calendar = Calendar.getInstance();

        // Initialize DateSetListener of DatePickerDialog
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                // Set the selected Date Info to Calendar instance
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                // Set Date Format
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

                // Set Date in input_dob EditText
                // datepick.setText(dateFormat.format(calendar.getTime()));
                String ss = dateFormat.format(calendar.getTime());
                Toast.makeText(getContext(), ss, Toast.LENGTH_SHORT).show();
                // getTimeSlots(ss);


            }
        };


        // Initialize DatePickerDialog
        DatePickerDialog datePicker = new DatePickerDialog
                (
                        getContext(),
                        date,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
        // datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        // Show datePicker Dialog
        datePicker.show();
    }
    private void availabledate() {
        Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.availabilitylayout);
        //firstlayer
        TextView fl1,fl2,fl3,fl4,fl5,fl6,fl7;
        //secondlayer
        TextView sl1,sl2,sl3,sl4,sl5,sl6,sl7;
        //thirdlayer
        TextView thl1,thl2,thl3,thl4,thl5,thl6,thl7;
        //fourthlayer
        TextView frl1,frl2,frl3,frl4,frl5,frl6,frl7;
        //fifthlayer
        TextView fifl1,fifl2,fifl3;
        fl1=dialog.findViewById(R.id.fl1);
        fl1.setText(today+"\n NA");
        fl2=dialog.findViewById(R.id.fl2);
        int nm = Integer.parseInt(today);
        int res = nm+1;
        if (res>nmmm){
            fl2.setText("");
        }else {
            fl2.setText(Integer.toString(res)+"\n NA");
        }

        fl3=dialog.findViewById(R.id.fl3);
        int res1 = nm+2;
        if (res1>nmmm){
            fl3.setText("");
        }else {
            fl3.setText(Integer.toString(res1)+"\n NA");
        }

        fl4=dialog.findViewById(R.id.fl4);
        int res2 = nm+3;
        if (res2>nmmm){
            fl4.setText("");
        }else {
            fl4.setText(Integer.toString(res2)+"\n NA");
        }

        fl5=dialog.findViewById(R.id.fl5);
        int res3 = nm+4;
        if (res3>nmmm){
            fl5.setText("");
        }else {
            fl5.setText(Integer.toString(res3)+"\n NA");
        }

        fl6=dialog.findViewById(R.id.fl6);
        int res4 = nm+5;
        if (res4>nmmm){
            fl6.setText("");
        }else {
            fl6.setText(Integer.toString(res4)+"\n NA");
        }

        fl7=dialog.findViewById(R.id.fl7);
        int res5 = nm+6;
        if (res5>nmmm){
            fl7.setText("");
        }else {
            fl7.setText(Integer.toString(res5)+"\n NA");
        }


        sl1=dialog.findViewById(R.id.sl1);
        int res6 = nm+7;
        if (res6>nmmm){
            sl1.setText("");
        }else {
            sl1.setText(Integer.toString(res6)+"\n NA");
        }

        sl2=dialog.findViewById(R.id.sl2);
        int res7 = nm+8;
        if (res7>nmmm){
            sl2.setText("");
        }else {
            sl2.setText(Integer.toString(res7)+"\n NA");
        }

        sl3=dialog.findViewById(R.id.sl3);
        int res8 = nm+9;
        if (res8>nmmm){
            sl3.setText("");
        }else {
            sl3.setText(Integer.toString(res8)+"\n NA");
        }

        sl4=dialog.findViewById(R.id.sl4);
        int res9 = nm+10;
        if (res9>nmmm){
            sl4.setText("");
        }else {
            sl4.setText(Integer.toString(res9)+"\n NA");
        }

        sl5=dialog.findViewById(R.id.sl5);
        int res10 = nm+11;
        if (res10>nmmm){
            sl5.setText("");
        }else {
            sl5.setText(Integer.toString(res10)+"\n NA");
        }

        sl6=dialog.findViewById(R.id.sl6);
        int res11 = nm+12;
        if (res11>nmmm){
            sl6.setText("");
        }else {
            sl6.setText(Integer.toString(res11)+"\n NA");
        }


        sl7=dialog.findViewById(R.id.sl7);
        int res12 = nm+13;
        if (res12>nmmm){
            sl7.setText("");
        }else {
            sl7.setText(Integer.toString(res12)+"\n NA");
        }
        thl1=dialog.findViewById(R.id.thl1);
        int res13 = nm+14;
        if (res13>nmmm){
            thl1.setText("");
        }else {
            thl1.setText(Integer.toString(res13)+"\n NA");
        }

        thl2=dialog.findViewById(R.id.thl2);
        int res14 = nm+15;
        if (res14>nmmm){
            thl2.setText("");
        }else {
            thl2.setText(Integer.toString(res14)+"\n NA");
        }

        thl3=dialog.findViewById(R.id.thl3);
        int res15 = nm+16;
        if (res15>nmmm){
            thl3.setText("");
        }else {
            thl3.setText(Integer.toString(res15)+"\n NA");
        }

        thl4=dialog.findViewById(R.id.thl4);
        int res16 = nm+17;
        if (res16>nmmm){
            thl4.setText("");
        }else {
            thl4.setText(Integer.toString(res16)+"\n NA");
        }

        thl5=dialog.findViewById(R.id.thl5);
        int res17 = nm+18;
        if (res17>nmmm){
            thl5.setText("");
        }else {thl5.setText(Integer.toString(res17)+"\n NA");}

        thl6=dialog.findViewById(R.id.thl6);
        int res18 = nm+19;
        if (res18>nmmm){
            thl6.setText("");
        }else {
            thl6.setText(Integer.toString(res18)+"\n NA");
        }

        thl7=dialog.findViewById(R.id.thl7);
        int res19 = nm+20;
        if (res19>nmmm){
            thl7.setText("");
        }else {
            thl7.setText(Integer.toString(res19)+"\n NA");
        }

        frl1=dialog.findViewById(R.id.frl1);
        int res20 = nm+21;
        if (res20>nmmm){
            frl1.setText("");
        }else {
            frl1.setText(Integer.toString(res20)+"\n NA");
        }

        frl2=dialog.findViewById(R.id.frl2);
        int res21 = nm+22;
        if (res21>nmmm){
            frl2.setText("");
        }else {
            frl2.setText(Integer.toString(res21)+"\n NA");
        }


        frl3=dialog.findViewById(R.id.frl3);
        int res22 = nm+23;
        if (res22>nmmm){
            frl3.setText("");
        }else {
            frl3.setText(Integer.toString(res22)+"\n NA");
        }

        frl4=dialog.findViewById(R.id.frl4);
        int res23 = nm+24;
        if (res23>nmmm){
            frl4.setText("");
        }else {
            frl3.setText(Integer.toString(res23)+"\n NA");
        }

        frl5=dialog.findViewById(R.id.frl5);
        int res24 = nm+25;
        if (res24>nmmm){
            frl5.setText("");
        }else {
            frl5.setText(Integer.toString(res24)+"\n NA");
        }
        frl6=dialog.findViewById(R.id.frl6);
        int res25 = nm+26;
        if (res25>nmmm){
            frl6.setText("");
        }else {
            frl6.setText(Integer.toString(res25)+"\n NA");
        }

        frl7=dialog.findViewById(R.id.frl7);
        int res26 = nm+27;
        if (res26>nmmm){
            frl7.setText("");
        }else {
            frl7.setText(Integer.toString(res26)+"\n NA");
        }


        fifl1=dialog.findViewById(R.id.fifl1);
        int res27 = nm+28;
        if (res27>nmmm){
            fifl1.setText("");
        }else {
            fifl1.setText(Integer.toString(res27)+"\n NA");
        }

        fifl2=dialog.findViewById(R.id.fifl2);
        int res28 = nm+29;
        if (res28>nmmm){
            fifl2.setText("");
        }else {
            fifl2.setText(Integer.toString(res28)+"\n NA");
        }
        fifl3=dialog.findViewById(R.id.fifl3);
        int res29 = nm+30;
        if (res29>nmmm){
            fifl3.setText("");
        }else {
            fifl3.setText(Integer.toString(res29)+"\n NA");
        }
        dialog.show();
    }
    private void placeOrder(String tagv,String wrkid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",wrkid);
        jsonObject.addProperty("tag_id",tagv);
        jsonObject.addProperty("no_of_days",selected);
        jsonObject.addProperty("order_start_date",startdate);
        // Toast.makeText(this, selected+" "+wrkid+" "+tagv, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<PlaceOrder_Repo> call = jsonpost.placeorder(jsonObject);
        call.enqueue(new Callback<PlaceOrder_Repo>() {
            @Override
            public void onResponse(Call<PlaceOrder_Repo> call, Response<PlaceOrder_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    assert response.body() != null;
                    String s = response.body().getMessage();
                    Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPreferences =getContext().getSharedPreferences("SharedTag",MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove("tag");
                    editor.apply();
                    Intent success = new Intent(getContext(), SuccessFull.class);
                    startActivity(success);

                }else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "else", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlaceOrder_Repo> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "onfail "+t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void postShortlis(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("task","add");
        // Toast.makeText(Skills.this, userid, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<ShortlistRepo> call = jsonpost.postshort(jsonObject);
        call.enqueue(new Callback<ShortlistRepo>() {
            @Override
            public void onResponse(Call<ShortlistRepo> call, Response<ShortlistRepo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    assert response.body() != null;
                    String s = response.body().getMessage();
                    Toast.makeText(getContext(),s, Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "else", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShortlistRepo> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void removeItem(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("task","remove");
        Toast.makeText(getContext(), workerid+" "+userid, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<RemoveFave_Repo> call = jsonpost.removefav(jsonObject);
        call.enqueue(new Callback<RemoveFave_Repo>() {
            @Override
            public void onResponse(Call<RemoveFave_Repo> call, Response<RemoveFave_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    String msg = response.body().getMessage();
                    UsersDetails();
                    usersadapter.notifyDataSetChanged();
                    Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(),"else  "+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RemoveFave_Repo> call, Throwable t) {
                Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void addToFaverate(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("task","add");
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<AddFave_Repo> call = jsonpost.addfaverate(jsonObject);
        call.enqueue(new Callback<AddFave_Repo>() {
            @Override
            public void onResponse(Call<AddFave_Repo> call, Response<AddFave_Repo> response) {
                if (response.isSuccessful()){
                    String s = response.body().getMessage();
                    UsersDetails();
                    usersadapter.notifyDataSetChanged();
                    Toast.makeText(getContext(),s, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddFave_Repo> call, Throwable t) {

            }
        });
    }
    private void getTags() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<TagsGet_Repo> call = jsonpost.gettagslist(data);
        call.enqueue(new Callback<TagsGet_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<TagsGet_Repo> call, retrofit2.Response<TagsGet_Repo> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    dataLists1 = response.body().getData();
                    for (int i=0;i<dataLists1.size();i++){
                        String tg = dataLists1.get(i).getTag();
                        String tgid = dataLists1.get(i).getId();
                        taglist.add(tg);
                        tagid.add(tgid);
                        arrayAdapter=new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, taglist);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                    }

                    if (response.body().getData().size()==0){

                    }else {

                    }

                }else {

                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<TagsGet_Repo> call, Throwable t) {
                //  Log.d("onfail",String.valueOf(t.getMessage()));

            }
        });
    }


    private void AllSkill() {
        progressDialog.setTitle("Loading....");
        progressDialog.setMessage("Fetching data from server");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setMax(20);
        progressDialog.show();
        recyclerViewall.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        RetrofitInterface getskillhere = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<All_Skill> call = getskillhere.getAll();
        call.enqueue(new Callback<All_Skill>() {
            @Override
            public void onResponse(Call<All_Skill> call, Response<All_Skill> response) {
                if (response.isSuccessful()){
                    allSkills=response.body().getData();
                    if (getActivity()!=null){
                        skill_adapter=new AllSkill_Adapter(getContext(),allSkills);
                        GridLayoutManager manager = new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false);
                        recyclerViewall.setLayoutManager(manager);
                        // recyclerViewall.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerViewall.setHasFixedSize(true);
                        recyclerViewall.setAdapter(skill_adapter);
                        progressDialog.dismiss();
                    }else {
                        progressDialog.dismiss();
                        recyclerViewall.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    progressDialog.dismiss();
                    recyclerViewall.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<All_Skill> call, Throwable t) {
                progressDialog.dismiss();

            }
        });
    }

    private AdapterView.OnItemClickListener onItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Toast.makeText(getContext(),
                            "Clicked item "
                                    + adapterView.getItemAtPosition(i)
                            , Toast.LENGTH_SHORT).show();
                    // item = (String) adapterView.getItemAtPosition(i);
                    UsersDetails();

                }
            };

    @Override
    public void onRefresh() {

    }
}
