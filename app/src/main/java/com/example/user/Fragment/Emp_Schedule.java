package com.example.user.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.Activity.Faverate;
import com.example.user.Activity.Shortlist;
import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.R;
import com.example.user.Response.Emp_Sch_Data;
import com.example.user.Response.Emp_Sch_Repo;
import com.example.user.Response.ShortgetList_Repo;
import com.example.user.Response.StatusChange_Repo;
import com.example.user.adapter.Schedule_Adapter;
import com.example.user.adapter.Schedule_List_Adapter;
import com.example.user.adapter.Shortlist_Adapter;
import com.example.user.model.Emp_Sch_Data_Orders;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Emp_Schedule extends Fragment {
     RecyclerView recyclerView;
    SharedPreferenceClass sharedPreferenceClass;
    String userid;
    private Schedule_Adapter schedule_adapter;
    List<Emp_Sch_Data> dataLists;
    MaterialSpinner spinner;
    String OrderId;
    JsonObject jsonObject;
    private String date,today;
    Integer nmmm;
    int MY_PERMISSIONS_REQUEST_CALL_PHONE=101;
    RecyclerView schedulelistadapter;
    Schedule_List_Adapter schedule_list_adapter;
    SwipeRefreshLayout pulltoreferesh;
    public Emp_Schedule() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_emp__schedule, container, false);
        recyclerView=view.findViewById(R.id.schedulrecycle);
        pulltoreferesh = view.findViewById(R.id.pulltoreferesh);
        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userid = sharedPreferenceClass.getValue_string("user_id");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        today = dayFormat.format(new Date());
        nmmm=  sdf.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
        schedulelistadapter=view.findViewById(R.id.schedulelistadapter);

        pulltoreferesh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSchedule();
            }
        });
        getSchedule();

        return view;
    }

    private void getSchedule() {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("order_status","Scheduled");
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Emp_Sch_Repo> call = jsonpost.getshedule(jsonObject);
        call.enqueue(new Callback<Emp_Sch_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<Emp_Sch_Repo> call, retrofit2.Response<Emp_Sch_Repo> response) {
                if (response.isSuccessful()){
                    pulltoreferesh.setRefreshing(false);
                    assert response.body() != null;
                    dataLists = response.body().getData();
                    schedule_adapter=new Schedule_Adapter(getContext(),dataLists);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(schedule_adapter);
                  /*  for (int i=0;i<dataLists.size();i++){
                        schedule_list_adapter=new Schedule_List_Adapter(dataLists.get(i).getOrders(),getContext());
                        schedulelistadapter.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        schedulelistadapter.setHasFixedSize(true);
                        schedulelistadapter.setAdapter(schedule_list_adapter);
                    }*/

                    if (response.body().getData().size()==0){
                        recyclerView.setVisibility(View.GONE);
                        pulltoreferesh.setRefreshing(false);
                        // nodata.setVisibility(View.VISIBLE);
                    }else {
                        pulltoreferesh.setRefreshing(false);
                        // nodata.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                }else {
                    pulltoreferesh.setRefreshing(false);
                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<Emp_Sch_Repo> call, Throwable t) {
                Log.d("onfail",String.valueOf(t.getMessage()));
                pulltoreferesh.setRefreshing(false);

            }
        });
    }
    private void showUpdateDialog() {
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
        alertdialog.setTitle("Update Order");
        alertdialog.setMessage("Please Choose Status");

        LayoutInflater inflater = this.getLayoutInflater();
        final View view = inflater.inflate(R.layout.orderspinnerlayout,null);
        spinner = view.findViewById(R.id.statusspinner);
        spinner.setItems("Cancelled","In Progress");
        alertdialog.setView(view);
        alertdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                String selectedstatus = String.valueOf(spinner.getText());
                //changeStatus(OrderId,selectedstatus);
               // item.setStatus(String.valueOf(spinner.getSelectedIndex()));

            }
        });

        alertdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertdialog.show();
    }
    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences1 =getContext().getSharedPreferences("callended", MODE_PRIVATE);
        String sss = sharedPreferences1.getString("callend", null);
        if (sss!=null){
            final Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.aftercalllayout);
            TextView textView = dialog.findViewById(R.id.ok);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences11 =getContext().getSharedPreferences("callended",MODE_PRIVATE);
                    SharedPreferences.Editor editor11 = sharedPreferences11.edit();
                    editor11.remove("callend");
                    editor11.clear();
                    editor11.apply();

                    SharedPreferences sharedPreferences =getContext().getSharedPreferences("Callshared", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove("Clicked");
                    editor.clear();
                    editor.apply();
                    dialog.dismiss();
                }
            });
            dialog.show();
            SharedPreferences sharedPreferences11 = getContext().getSharedPreferences("callended",MODE_PRIVATE);
            SharedPreferences.Editor editor11 = sharedPreferences11.edit();
            editor11.remove("callend");
            editor11.clear();
            editor11.apply();
        }
    }
}
