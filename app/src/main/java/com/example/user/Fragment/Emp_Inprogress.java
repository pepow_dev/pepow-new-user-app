package com.example.user.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.R;
import com.example.user.Response.Emp_Sch_Data;
import com.example.user.Response.Emp_Sch_Repo;
import com.example.user.Response.Track_Imp_DataOrders;
import com.example.user.Response.Track_Inp_Data;
import com.example.user.Response.Track_Inp_Repo;
import com.example.user.adapter.Inprogress_Adapter;
import com.example.user.adapter.Inprogress_List_Adapter;
import com.example.user.adapter.Schedule_Adapter;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Emp_Inprogress extends Fragment {
    RecyclerView recyclerView;
    SharedPreferenceClass sharedPreferenceClass;
    String userid;
    private Inprogress_Adapter schedule_adapter;
    List<Track_Inp_Data> dataLists;
    List<Track_Imp_DataOrders> orderslist;
    Inprogress_List_Adapter inprogress_list_adapter;
    MaterialSpinner spinner;
    String OrderId;
    SwipeRefreshLayout swipeRefreshLayout;
    JsonObject jsonObject;
    private String date,today;
    Integer nmmm;
    RecyclerView tracklist;
    int MY_PERMISSIONS_REQUEST_CALL_PHONE=101;
    public Emp_Inprogress() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_emp__inprogress, container, false);
        recyclerView=view.findViewById(R.id.inprogressrecycle);
        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userid = sharedPreferenceClass.getValue_string("user_id");
        swipeRefreshLayout=view.findViewById(R.id.swipe);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        today = dayFormat.format(new Date());
        nmmm=  sdf.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getInprogress();
            }
        });
        tracklist=view.findViewById(R.id.trackinplist);
        getInprogress();
        return view;
    }



    private void getInprogress() {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("order_status","In Progress");
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Track_Inp_Repo> call = jsonpost.getinprogress(jsonObject);
        call.enqueue(new Callback<Track_Inp_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<Track_Inp_Repo> call, retrofit2.Response<Track_Inp_Repo> response) {
                if (response.isSuccessful()){
                    swipeRefreshLayout.setRefreshing(false);
                    assert response.body() != null;
                    dataLists = response.body().getData();
                    schedule_adapter=new Inprogress_Adapter(getContext(),dataLists);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(schedule_adapter);
                    for (int i=0;i<dataLists.size();i++){
                       // inprogress_list_adapter= new Inprogress_List_Adapter(dataLists.get(i).getOrders(),getContext());
                       // tracklist.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                       // tracklist.setHasFixedSize(true);
                       // tracklist.setAdapter(inprogress_list_adapter);

                    }


                  /*  schedule_adapter.setOnITemClickListener(new Inprogress_Adapter.OnItemClickListener() {
                        @Override
                        public void onViewClick(int position) {
                         //   showUpdateDialog();

                                    OrderId = dataLists.get(position).getOrders().get(0).getId();



                        }


                        @Override
                        public void onClickAvailable(int position) {
                            availabledate();

                        }

                        @Override
                        public void onCallClick(int position) {
                            SharedPreferences sharedPreferences = getContext().getSharedPreferences("Callshared",MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("Clicked","call");
                            editor.apply();
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                              String  Phonenumber = dataLists.get(position).getOrders().get(0).getPhone_no();
                                callIntent.setData(Uri.parse("tel:"+Phonenumber));



                            if (ActivityCompat.checkSelfPermission(getContext(),android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(),
                                        android.Manifest.permission.CALL_PHONE)) {
                                } else {
                                    ActivityCompat.requestPermissions((Activity) getContext(),
                                            new String[]{android.Manifest.permission.CALL_PHONE},
                                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                                }
                            }
                            getContext().startActivity(callIntent);
                        }

                        @Override
                        public void onExtendClick(int position) {
                         datePicker();
                        }

                        @Override
                        public void onPaymentHistoryClick(int position) {
                            Dialog dialog = new Dialog(getContext());
                            dialog.setContentView(R.layout.paymenthistory);
                            dialog.show();
                        }
                    });*/
                    if (response.body().getData().size()==0){
                        recyclerView.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        // nodata.setVisibility(View.VISIBLE);
                    }else {
                        swipeRefreshLayout.setRefreshing(false);
                        // nodata.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                }else {
                    swipeRefreshLayout.setRefreshing(false);
                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<Track_Inp_Repo> call, Throwable t) {
                Log.d("onfail",String.valueOf(t.getMessage()));
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }
    private void availabledate() {
        Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.availabilitylayout);
        //firstlayer
        TextView fl1,fl2,fl3,fl4,fl5,fl6,fl7;
        //secondlayer
        TextView sl1,sl2,sl3,sl4,sl5,sl6,sl7;
        //thirdlayer
        TextView thl1,thl2,thl3,thl4,thl5,thl6,thl7;
        //fourthlayer
        TextView frl1,frl2,frl3,frl4,frl5,frl6,frl7;
        //fifthlayer
        TextView fifl1,fifl2,fifl3;
        fl1=dialog.findViewById(R.id.fl1);
        fl1.setText(today+"\n NA");
        fl2=dialog.findViewById(R.id.fl2);
        int nm = Integer.parseInt(today);
        int res = nm+1;
        if (res>nmmm){
            fl2.setText("");
        }else {
            fl2.setText(Integer.toString(res)+"\n NA");
        }

        fl3=dialog.findViewById(R.id.fl3);
        int res1 = nm+2;
        if (res1>nmmm){
            fl3.setText("");
        }else {
            fl3.setText(Integer.toString(res1)+"\n NA");
        }

        fl4=dialog.findViewById(R.id.fl4);
        int res2 = nm+3;
        if (res2>nmmm){
            fl4.setText("");
        }else {
            fl4.setText(Integer.toString(res2)+"\n NA");
        }

        fl5=dialog.findViewById(R.id.fl5);
        int res3 = nm+4;
        if (res3>nmmm){
            fl5.setText("");
        }else {
            fl5.setText(Integer.toString(res3)+"\n NA");
        }

        fl6=dialog.findViewById(R.id.fl6);
        int res4 = nm+5;
        if (res4>nmmm){
            fl6.setText("");
        }else {
            fl6.setText(Integer.toString(res4)+"\n NA");
        }

        fl7=dialog.findViewById(R.id.fl7);
        int res5 = nm+6;
        if (res5>nmmm){
            fl7.setText("");
        }else {
            fl7.setText(Integer.toString(res5)+"\n NA");
        }


        sl1=dialog.findViewById(R.id.sl1);
        int res6 = nm+7;
        if (res6>nmmm){
            sl1.setText("");
        }else {
            sl1.setText(Integer.toString(res6)+"\n NA");
        }

        sl2=dialog.findViewById(R.id.sl2);
        int res7 = nm+8;
        if (res7>nmmm){
            sl2.setText("");
        }else {
            sl2.setText(Integer.toString(res7)+"\n NA");
        }

        sl3=dialog.findViewById(R.id.sl3);
        int res8 = nm+9;
        if (res8>nmmm){
            sl3.setText("");
        }else {
            sl3.setText(Integer.toString(res8)+"\n NA");
        }

        sl4=dialog.findViewById(R.id.sl4);
        int res9 = nm+10;
        if (res9>nmmm){
            sl4.setText("");
        }else {
            sl4.setText(Integer.toString(res9)+"\n NA");
        }

        sl5=dialog.findViewById(R.id.sl5);
        int res10 = nm+11;
        if (res10>nmmm){
            sl5.setText("");
        }else {
            sl5.setText(Integer.toString(res10)+"\n NA");
        }

        sl6=dialog.findViewById(R.id.sl6);
        int res11 = nm+12;
        if (res11>nmmm){
            sl6.setText("");
        }else {
            sl6.setText(Integer.toString(res11)+"\n NA");
        }


        sl7=dialog.findViewById(R.id.sl7);
        int res12 = nm+13;
        if (res12>nmmm){
            sl7.setText("");
        }else {
            sl7.setText(Integer.toString(res12)+"\n NA");
        }
        thl1=dialog.findViewById(R.id.thl1);
        int res13 = nm+14;
        if (res13>nmmm){
            thl1.setText("");
        }else {
            thl1.setText(Integer.toString(res13)+"\n NA");
        }

        thl2=dialog.findViewById(R.id.thl2);
        int res14 = nm+15;
        if (res14>nmmm){
            thl2.setText("");
        }else {
            thl2.setText(Integer.toString(res14)+"\n NA");
        }

        thl3=dialog.findViewById(R.id.thl3);
        int res15 = nm+16;
        if (res15>nmmm){
            thl3.setText("");
        }else {
            thl3.setText(Integer.toString(res15)+"\n NA");
        }

        thl4=dialog.findViewById(R.id.thl4);
        int res16 = nm+17;
        if (res16>nmmm){
            thl4.setText("");
        }else {
            thl4.setText(Integer.toString(res16)+"\n NA");
        }

        thl5=dialog.findViewById(R.id.thl5);
        int res17 = nm+18;
        if (res17>nmmm){
            thl5.setText("");
        }else {thl5.setText(Integer.toString(res17)+"\n NA");}

        thl6=dialog.findViewById(R.id.thl6);
        int res18 = nm+19;
        if (res18>nmmm){
            thl6.setText("");
        }else {
            thl6.setText(Integer.toString(res18)+"\n NA");
        }

        thl7=dialog.findViewById(R.id.thl7);
        int res19 = nm+20;
        if (res19>nmmm){
            thl7.setText("");
        }else {
            thl7.setText(Integer.toString(res19)+"\n NA");
        }

        frl1=dialog.findViewById(R.id.frl1);
        int res20 = nm+21;
        if (res20>nmmm){
            frl1.setText("");
        }else {
            frl1.setText(Integer.toString(res20)+"\n NA");
        }

        frl2=dialog.findViewById(R.id.frl2);
        int res21 = nm+22;
        if (res21>nmmm){
            frl2.setText("");
        }else {
            frl2.setText(Integer.toString(res21)+"\n NA");
        }


        frl3=dialog.findViewById(R.id.frl3);
        int res22 = nm+23;
        if (res22>nmmm){
            frl3.setText("");
        }else {
            frl3.setText(Integer.toString(res22)+"\n NA");
        }

        frl4=dialog.findViewById(R.id.frl4);
        int res23 = nm+24;
        if (res23>nmmm){
            frl4.setText("");
        }else {
            frl3.setText(Integer.toString(res23)+"\n NA");
        }

        frl5=dialog.findViewById(R.id.frl5);
        int res24 = nm+25;
        if (res24>nmmm){
            frl5.setText("");
        }else {
            frl5.setText(Integer.toString(res24)+"\n NA");
        }
        frl6=dialog.findViewById(R.id.frl6);
        int res25 = nm+26;
        if (res25>nmmm){
            frl6.setText("");
        }else {
            frl6.setText(Integer.toString(res25)+"\n NA");
        }

        frl7=dialog.findViewById(R.id.frl7);
        int res26 = nm+27;
        if (res26>nmmm){
            frl7.setText("");
        }else {
            frl7.setText(Integer.toString(res26)+"\n NA");
        }


        fifl1=dialog.findViewById(R.id.fifl1);
        int res27 = nm+28;
        if (res27>nmmm){
            fifl1.setText("");
        }else {
            fifl1.setText(Integer.toString(res27)+"\n NA");
        }

        fifl2=dialog.findViewById(R.id.fifl2);
        int res28 = nm+29;
        if (res28>nmmm){
            fifl2.setText("");
        }else {
            fifl2.setText(Integer.toString(res28)+"\n NA");
        }
        fifl3=dialog.findViewById(R.id.fifl3);
        int res29 = nm+30;
        if (res29>nmmm){
            fifl3.setText("");
        }else {
            fifl3.setText(Integer.toString(res29)+"\n NA");
        }
        dialog.show();
    }
    private void datePicker() {
        // Get Calendar instance
        final Calendar calendar = Calendar.getInstance();

        // Initialize DateSetListener of DatePickerDialog
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                // Set the selected Date Info to Calendar instance
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                // Set Date Format
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

                // Set Date in input_dob EditText
                // datepick.setText(dateFormat.format(calendar.getTime()));
                //  selecteddate = dateFormat.format(calendar.getTime());
                // getTimeSlots(ss);


            }
        };


        // Initialize DatePickerDialog
        DatePickerDialog datePicker = new DatePickerDialog
                (
                        getContext(),
                        date,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
        // datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        // Show datePicker Dialog
        datePicker.show();
    }
    @Override
    public void onPause() {
        super.onPause();
        getInprogress();
    }
/* private void showUpdateDialog() {
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
        alertdialog.setTitle("Update Order");
        alertdialog.setMessage("Please Choose Status");

        LayoutInflater inflater = this.getLayoutInflater();
        final View view = inflater.inflate(R.layout.orderspinnerlayout,null);
        spinner = view.findViewById(R.id.statusspinner);
        spinner.setItems("Cancelled","In Progress");
        alertdialog.setView(view);
        alertdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                String selectedstatus = String.valueOf(spinner.getText());
                changeStatus(OrderId,selectedstatus);
                // item.setStatus(String.valueOf(spinner.getSelectedIndex()));

            }
        });

        alertdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertdialog.show();
    }*/

}
