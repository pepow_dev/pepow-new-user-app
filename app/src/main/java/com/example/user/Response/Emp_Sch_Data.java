package com.example.user.Response;

import com.example.user.model.Emp_Sch_Data_Orders;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Emp_Sch_Data {
    @SerializedName("orders")
    @Expose
    private List<Emp_Sch_Data_Orders> orders;
    @SerializedName("tag_name")
    @Expose
    private String tag_name;

    public List<Emp_Sch_Data_Orders> getOrders() {
        return orders;
    }

    public void setOrders(List<Emp_Sch_Data_Orders> orders) {
        this.orders = orders;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }
}
