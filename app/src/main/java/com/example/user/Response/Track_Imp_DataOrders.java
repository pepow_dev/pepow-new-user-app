package com.example.user.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Track_Imp_DataOrders {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("worker_id")
    @Expose
    private String worker_id;
    @SerializedName("order_no")
    @Expose
    private String order_no;
    @SerializedName("order_date")
    @Expose
    private String order_date;
    @SerializedName("order_start_date")
    @Expose
    private String order_start_date;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profile_pic")
    @Expose
    private String profile_pic;

    @SerializedName("phone_no")
    @Expose
    private String phone_no;

    @SerializedName("primary_skill")
    @Expose
    private String primary_skill;

    @SerializedName("secondary_skill")
    @Expose
    private String secondary_skill;

    @SerializedName("work_location")
    @Expose
    private String work_location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(String worker_id) {
        this.worker_id = worker_id;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_start_date() {
        return order_start_date;
    }

    public void setOrder_start_date(String order_start_date) {
        this.order_start_date = order_start_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPrimary_skill() {
        return primary_skill;
    }

    public void setPrimary_skill(String primary_skill) {
        this.primary_skill = primary_skill;
    }

    public String getSecondary_skill() {
        return secondary_skill;
    }

    public void setSecondary_skill(String secondary_skill) {
        this.secondary_skill = secondary_skill;
    }

    public String getWork_location() {
        return work_location;
    }

    public void setWork_location(String work_location) {
        this.work_location = work_location;
    }
}
