package com.example.user.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmOrder_data {
    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("no_of_days")
    @Expose
    private String no_of_days;

    @SerializedName("worker_id")
    @Expose
    private String worker_id;

    @SerializedName("tag_id")
    @Expose
    private String tag_id;

    @SerializedName("order_start_date")
    @Expose
    private String order_start_date;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNo_of_days() {
        return no_of_days;
    }

    public void setNo_of_days(String no_of_days) {
        this.no_of_days = no_of_days;
    }

    public String getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(String worker_id) {
        this.worker_id = worker_id;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getOrder_start_date() {
        return order_start_date;
    }

    public void setOrder_start_date(String order_start_date) {
        this.order_start_date = order_start_date;
    }
}
