package com.example.user.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShortgetList_Repo {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<ShortgetList_Data> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ShortgetList_Data> getData() {
        return data;
    }

    public void setData(List<ShortgetList_Data> data) {
        this.data = data;
    }
}
