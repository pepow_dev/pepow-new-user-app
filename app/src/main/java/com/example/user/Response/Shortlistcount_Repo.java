package com.example.user.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Shortlistcount_Repo {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Shortlistcount_data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Shortlistcount_data getData() {
        return data;
    }

    public void setData(Shortlistcount_data data) {
        this.data = data;
    }
}
