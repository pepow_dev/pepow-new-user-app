package com.example.user.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataList {
    @SerializedName("worker_id")
    @Expose
    private String worker_id;
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("phone_no")
    @Expose
    private String phone_no;

    @SerializedName("profile_pic")
    @Expose
    private String  profile_pic;

    @SerializedName("work_location")
    @Expose
    private String work_location;

    @SerializedName("expected_salary")
    @Expose
    private String expected_salary;

    @SerializedName("register_for")
    @Expose
    private String register_for;

    @SerializedName("secondary_skill")
    @Expose
    private String secondary_skill;

    @SerializedName("primary_skill")
    @Expose
    private String primary_skill;

    @SerializedName("rate_per_day")
    @Expose
    private String rate_per_day;

    @SerializedName("favourite")
    @Expose
    private String favourite;

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    public String getWork_location() {
        return work_location;
    }

    public void setWork_location(String work_location) {
        this.work_location = work_location;
    }

    public String getExpected_salary() {
        return expected_salary;
    }

    public void setExpected_salary(String expected_salary) {
        this.expected_salary = expected_salary;
    }

    public String getRegister_for() {
        return register_for;
    }

    public void setRegister_for(String register_for) {
        this.register_for = register_for;
    }

    public String getSecondary_skill() {
        return secondary_skill;
    }

    public void setSecondary_skill(String secondary_skill) {
        this.secondary_skill = secondary_skill;
    }

    public String getPrimary_skill() {
        return primary_skill;
    }

    public void setPrimary_skill(String primary_skill) {
        this.primary_skill = primary_skill;
    }

    public String getRate_per_day() {
        return rate_per_day;
    }

    public void setRate_per_day(String rate_per_day) {
        this.rate_per_day = rate_per_day;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(String worker_id) {
        this.worker_id = worker_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }
}
