package com.example.user.Response;

import com.example.user.model.Emp_Sch_Data_Orders;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Track_Inp_Data {

    @SerializedName("orders")
    @Expose
    private List<Track_Imp_DataOrders> orders;
    @SerializedName("tag_name")
    @Expose
    private String tag_name;

    public List<Track_Imp_DataOrders> getOrders() {
        return orders;
    }

    public void setOrders(List<Track_Imp_DataOrders> orders) {
        this.orders = orders;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }
}
