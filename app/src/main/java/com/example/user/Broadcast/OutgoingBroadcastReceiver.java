package com.example.user.Broadcast;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.user.R;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.WINDOW_SERVICE;

public class OutgoingBroadcastReceiver extends BroadcastReceiver {
    WindowManager windowManager2;
    View view;
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPreferences1 = context.getSharedPreferences("Callshared", MODE_PRIVATE);
        String sss = sharedPreferences1.getString("Clicked", null);
        if (sss != null) {
            if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                showToast(context, "Call started");
                //SharedPreferences sharedPreferences = context.getSharedPreferences("Callshared",MODE_PRIVATE);
                // SharedPreferences.Editor editor = sharedPreferences.edit();
                // editor.remove("Clicked");
                // editor.clear();
                // editor.apply();


            } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                showToast(context, "Call End");
                SharedPreferences sharedPreferences11 = context.getSharedPreferences("callended",MODE_PRIVATE);
                SharedPreferences.Editor editor11 = sharedPreferences11.edit();
                editor11.putString("callend","end");
                editor11.apply();
                SharedPreferences sharedPreferences = context.getSharedPreferences("Callshared", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("Clicked");
                editor.clear();
                editor.apply();

            }
        } else {
            Log.d("elsecall", "elsecall");
            //Toast.makeText(context, "check your number", Toast.LENGTH_SHORT).show();
        }
    }



    void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}

