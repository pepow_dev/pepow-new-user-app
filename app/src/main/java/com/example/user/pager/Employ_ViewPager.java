package com.example.user.pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.user.Fragment.Emp_Completed;
import com.example.user.Fragment.Emp_Inprogress;
import com.example.user.Fragment.Emp_Schedule;
import com.example.user.Fragment.Employement;
import com.example.user.Fragment.Fee_For_Service;

public class Employ_ViewPager extends FragmentPagerAdapter {
    public Employ_ViewPager(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        if (position == 0)
        {
            fragment = new Emp_Schedule();
        }
        else if (position == 1)
        {
            fragment = new Emp_Inprogress();
        }
        else if (position == 2)
        {
            fragment = new Emp_Completed();
        }

        return fragment;
    }



    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Schedule";

        }
        else if (position == 1)
        {
            title = "InProgress";
        }
        else if (position == 2)
        {
            title = "Completed";
        }

        return title;
    }
}
