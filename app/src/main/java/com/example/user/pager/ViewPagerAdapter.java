package com.example.user.pager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.example.user.Fragment.Employement;
import com.example.user.Fragment.Fee_For_Service;
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        if (position == 0)
        {
            fragment = new Fee_For_Service();
        }
        else if (position == 1)
        {
            fragment = new Employement();
        }

        return fragment;
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Fee For Service";

        }
        else if (position == 1)
        {
            title = "Employment";
        }

        return title;
    }
}
