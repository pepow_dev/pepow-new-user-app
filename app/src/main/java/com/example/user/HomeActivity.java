package com.example.user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;

import com.example.user.Activity.SearchLocation;
import com.example.user.Activity.Shortlist;
import com.example.user.Activity.Skills;
import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.Response.Shortlistcount_Repo;
import com.example.user.location.SamLocationRequestService;
import com.example.user.pager.ViewPagerAdapter;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.android.material.tabs.TabLayout;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.user.Activity.SearchLocation.SHARED_PREFS;

public class HomeActivity extends MainActivity {
    FrameLayout contentFrameLayout;
    private SamLocationRequestService samLocationRequestService;
    private int REQUEST_CODE=1000;
    Geocoder geocoder;
    String city,ss,sname,locality,userid;
    List<Address> addresses;
    Button button;
    Bundle extras;
    ProgressBar progressBar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    SharedPreferences sharedPreferences;
    TextView name,textonly,count;
    ImageView carticon;

    private SharedPreferenceClass sharedPreferenceClass;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_home,contentFrameLayout);
        extras = getIntent().getExtras();
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(this);
        button = toolbar.findViewById(R.id.button);
        progressBar= toolbar.findViewById(R.id.toolbar_progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
        tabLayout=findViewById(R.id.tabLayout2);
        viewPager=findViewById(R.id.viewpager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        name=findViewById(R.id.name);
        textonly=findViewById(R.id.textonly);
        count=toolbar.findViewById(R.id.count);
        sharedPreferenceClass = new SharedPreferenceClass(HomeActivity.this);
        sname = sharedPreferenceClass.getValue_string("fname");
        userid = sharedPreferenceClass.getValue_string("user_id");
        sharedPreferences = getSharedPreferences("userinfo",Context.MODE_PRIVATE);
        Typeface font = Typeface.createFromAsset(getAssets(),"font/Uber Move Text.ttf");
        name.setText("Hello "+sname);
        name.setTypeface(font);
        textonly.setTypeface(font);
        carticon=toolbar.findViewById(R.id.carticon);
        carticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shortlist = new Intent(HomeActivity.this, Shortlist.class);
                startActivity(shortlist);
                finish();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchLocation.class);
                startActivity(intent);
                finish();
            }
        });

        getSupportActionBar().setTitle(null);
        geocoder = new Geocoder(this, Locale.getDefault());
        tool_location();
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);


        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));

        getshortlistcount();
    }

    private void getshortlistcount() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Shortlistcount_Repo>call = jsonpost.getshortlistcount(data);
        call.enqueue(new Callback<Shortlistcount_Repo>() {
            @Override
            public void onResponse(Call<Shortlistcount_Repo> call, Response<Shortlistcount_Repo> response) {
                if (response.isSuccessful()){
                    Log.d("if",response.body().getMessage());
                    String counting = response.body().getData().getCount();
                    count.setText(counting);
                }else {
                }
            }

            @Override
            public void onFailure(Call<Shortlistcount_Repo> call, Throwable t) {
                Toast.makeText(HomeActivity.this, "fail", Toast.LENGTH_SHORT).show();
                Log.d("faaail",t.getMessage());
            }
        });
    }

    private void tool_location() {
        samLocationRequestService=new SamLocationRequestService(HomeActivity.this, new SamLocationRequestService.SamLocationListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onLocationUpdate(Location location, Address address) {
                try{
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    SharedPreferences sharedPreferences1 = getSharedPreferences("sharedlocation",MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences1.edit();
                 city = addresses.get(0).getLocality();
                 locality=addresses.get(0).getSubLocality();
                   // Toast.makeText(HomeActivity.this, locality, Toast.LENGTH_SHORT).show();
                 double lat = addresses.get(0).getLatitude();
                 double longi = addresses.get(0).getLongitude();
                 Location location1 = new Location("");
                 location1.setLatitude(lat);
                 location1.setLongitude(longi);
                 editor.putString("lat", String.valueOf(lat));
                    editor.putString("longi", String.valueOf(longi));

                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                   ss = sharedPreferences.getString("location",null);

                 if (ss == null){
                     progressBar.setVisibility(View.GONE);
                     button.setVisibility(View.VISIBLE);
                     toolbar.setSubtitle(locality+","+city);
                     editor.putString("location",locality+","+city);
                     editor.apply();
                     toolbar.setSubtitleTextAppearance(HomeActivity.this, R.style.PagerTabStripText);
                 }else {
                     progressBar.setVisibility(View.GONE);
                     button.setVisibility(View.VISIBLE);
                   //  newString= extras.getString("place");
                     toolbar.setSubtitle(ss);
                     editor.putString("location",ss);
                     editor.apply();
                    // Toast.makeText(this, newString, Toast.LENGTH_SHORT).show();
                 }
                    toolbar.setTitle("Your location");
                    toolbar.setTitleTextColor(getColor(R.color.white11));
                    toolbar.setTitleTextAppearance(HomeActivity.this, R.style.PagerTabStripText);

                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        },REQUEST_CODE);
    }

        private void setWindowFlag(HomeActivity homeActivity, final int bits, boolean on) {
            Window win = homeActivity.getWindow();

            WindowManager.LayoutParams winParams = win.getAttributes();
            if (on) {
                winParams.flags |= bits;
            } else {
                winParams.flags &= ~bits;
            }
            win.setAttributes(winParams);
        }
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE){
                samLocationRequestService.startLocationUpdates();
            }
        }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences1 = getSharedPreferences("callended", MODE_PRIVATE);
        String sss = sharedPreferences1.getString("callend", null);
        if (sss!=null){
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.aftercalllayout);
            TextView textView = dialog.findViewById(R.id.ok);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences11 = getSharedPreferences("callended",MODE_PRIVATE);
                    SharedPreferences.Editor editor11 = sharedPreferences11.edit();
                    editor11.remove("callend");
                    editor11.clear();
                    editor11.apply();

                    SharedPreferences sharedPreferences =getSharedPreferences("Callshared", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove("Clicked");
                    editor.clear();
                    editor.apply();
                    dialog.dismiss();
                }
            });
            dialog.show();
            SharedPreferences sharedPreferences11 = getSharedPreferences("callended",MODE_PRIVATE);
            SharedPreferences.Editor editor11 = sharedPreferences11.edit();
            editor11.remove("callend");
            editor11.clear();
            editor11.apply();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
