package com.example.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.user.R;
import com.example.user.Response.Data_AllSkill;

import java.util.List;

public class Card extends RecyclerView.Adapter<Card.CardViewHolder> {
    private LayoutInflater inflater;
    private List<Data_AllSkill> modelRecyclerArrayList1;
    Context ctx;

    public Card( Context ctx,List<Data_AllSkill> modelRecyclerArrayList1) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList1 = modelRecyclerArrayList1;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.cardlayout, parent, false);
        CardViewHolder holder = new CardViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        Glide.with(ctx).load(modelRecyclerArrayList1.get(position).getImage()).into(holder.skillimage);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        ImageView skillimage;
        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            skillimage = itemView.findViewById(R.id.imagecard);
        }
    }
}
