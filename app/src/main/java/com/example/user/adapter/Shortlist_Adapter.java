package com.example.user.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.example.user.R;
import com.example.user.Response.Data_AllSkill;
import com.example.user.Response.ShortgetList_Data;
import com.example.user.Response.ShortgetList_Repo;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Shortlist_Adapter extends RecyclerView.Adapter<Shortlist_Adapter.ShrtlistViewHolder> {
    private LayoutInflater inflater;
    private List<ShortgetList_Data> shortgetList_repos;
    Context ctx;
    OnItemTouchListener monItemTouch;
    OnItemClickListener mListener;
    public interface OnItemClickListener{
        void ondeleteClick(int position);
        void onplaceorderClick(int position);
        void onAddFaverateClick(int position);
        void onRemoveFaverateClick(int position);
        void onCallButtonClick(int position);
        void onDatepickerClick(int position);
        void oncheckavailableClick(int position);

    }

    public void setOnITemClickListener(OnItemClickListener listener){
        mListener = listener;
    }



    public interface OnItemTouchListener{
        void OnitemTouch(int position);
    }
public void OnItemTouch(OnItemTouchListener onItemTouchListener){
    monItemTouch=onItemTouchListener;

}
    public Shortlist_Adapter(Context ctx,List<ShortgetList_Data> shortgetList_repos) {
        this.inflater = inflater.from(ctx);
        this.shortgetList_repos = shortgetList_repos;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public Shortlist_Adapter.ShrtlistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.shortlist_layout, parent, false);
        ShrtlistViewHolder holder = new ShrtlistViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Shortlist_Adapter.ShrtlistViewHolder holder, int position) {
        holder.fname.setText(shortgetList_repos.get(position).getFirst_name());
        holder.lname.setText(shortgetList_repos.get(position).getLast_name());
        String priskill = shortgetList_repos.get(position).getPrimary_skill();
        String scskill = shortgetList_repos.get(position).getSecondary_skill();
        holder.skill.setText(priskill+","+scskill);
        holder.rateperday.setText(shortgetList_repos.get(position).getRate_per_day()+" /day or hr");
        holder.visitingchrg.setText("Visit charge:"+shortgetList_repos.get(position).getExpected_salary());
        holder.area.setText(shortgetList_repos.get(position).getArea());
        String phone = shortgetList_repos.get(position).getPhone_no();
        String workerid = shortgetList_repos.get(position).getWorker_id();
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener(){
            @Override
            public void onOpen(SwipeLayout layout) {
                super.onOpen(layout);
            }
        });



        Glide.with(ctx).load(shortgetList_repos.get(position).getProfile_pic()).into(holder.imageView);
        String faveratestatus = shortgetList_repos.get(position).getIs_favourite();
        if (faveratestatus.equals("0")){
            holder.favicon.setVisibility(View.VISIBLE);
            holder.refav.setVisibility(View.GONE);
        }else if (faveratestatus.equals("1")){
            holder.favicon.setVisibility(View.GONE);
            holder.refav.setVisibility(View.VISIBLE);
        }
        String cnfrmstatus = shortgetList_repos.get(position).getConfirmed_order();
        if (cnfrmstatus.equals("0")){
            holder.check.setVisibility(View.GONE);
        }else if (cnfrmstatus.equals("1")){
            holder.check.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return shortgetList_repos.size();
    }

    public class ShrtlistViewHolder extends RecyclerView.ViewHolder {
        TextView fname,lname,skill,rateperday,visitingchrg,area,checkavailable;
        CircularImageView imageView;
        ImageView phone,delete,calender,favicon,refav,check;
        Button placeorder;
        private SwipeLayout swipeLayout;

        public ShrtlistViewHolder(@NonNull View itemView) {
            super(itemView);
            fname=itemView.findViewById(R.id.firstname);
            lname=itemView.findViewById(R.id.lastname);
            skill=itemView.findViewById(R.id.userid);
            area=itemView.findViewById(R.id.area);
            rateperday=itemView.findViewById(R.id.rateperday);
            visitingchrg=itemView.findViewById(R.id.visitingcharge);
            imageView=itemView.findViewById(R.id.profileimage);
            delete=itemView.findViewById(R.id.delete);
            placeorder=itemView.findViewById(R.id.placeorder);
           phone=itemView.findViewById(R.id.call);
           calender=itemView.findViewById(R.id.calender);
            checkavailable=itemView.findViewById(R.id.checkavailable);
            favicon=itemView.findViewById(R.id.faverate);
            refav=itemView.findViewById(R.id.redfaverate);
            check=itemView.findViewById(R.id.check);

            swipeLayout=(SwipeLayout) itemView.findViewById(R.id.id_swipe);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            fname.setTypeface(font);
            lname.setTypeface(font);
            skill.setTypeface(font);
            area.setTypeface(font);
            rateperday.setTypeface(font);
            visitingchrg.setTypeface(font);
            placeorder.setTypeface(font);
            checkavailable.setTypeface(font);



            favicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onAddFaverateClick(position);
                        }
                    }
                }
            });

            refav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onRemoveFaverateClick(position);
                        }
                    }
                }
            });


            calender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onDatepickerClick(position);
                        }
                    }
                }
            });
            checkavailable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.oncheckavailableClick(position);
                        }
                    }
                }
            });
            phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onCallButtonClick(position);
                        }
                    }
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.ondeleteClick(position);
                        }
                    }
                }
            });
            placeorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onplaceorderClick(position);
                        }
                    }
                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (monItemTouch !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            monItemTouch.OnitemTouch(position);
                        }
                    }

                }

            });


        }
    }

}
