package com.example.user.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.user.R;
import com.example.user.Response.Track_Imp_DataOrders;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Inprogress_List_Adapter extends RecyclerView.Adapter<Inprogress_List_Adapter.InprogressViewHolder> {
    private LayoutInflater inflater;
    private List<Track_Imp_DataOrders> emp_list_data;
    Context ctx;

    public Inprogress_List_Adapter(List<Track_Imp_DataOrders> emp_list_data, Context ctx) {
        this.inflater = inflater.from(ctx);
        this.emp_list_data = emp_list_data;
        this.ctx = ctx;
    }


    @NonNull
    @Override
    public InprogressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.track_inp_list, parent, false);
        InprogressViewHolder holder = new InprogressViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InprogressViewHolder holder, int position) {
        holder.workerid.setText("#"+emp_list_data.get(position).getOrder_no());
        holder.date.setText(emp_list_data.get(position).getOrder_date());
        holder.empdate.setText(emp_list_data.get(position).getOrder_start_date());
        holder.name.setText(emp_list_data.get(position).getName());
        holder.skill.setText(emp_list_data.get(position).getPrimary_skill()+","+emp_list_data.get(position).getSecondary_skill());
        holder.location.setText(emp_list_data.get(position).getWork_location());
        Picasso.with(ctx).load(emp_list_data.get(position).getProfile_pic()).into(holder.profileimage);


       holder.workerid.setText(emp_list_data.get(position).getWorker_id());
        Picasso.with(ctx).load(emp_list_data.get(position).getProfile_pic()).into(holder.profileimage);
    }

    @Override
    public int getItemCount() {
        return emp_list_data.size();
    }

    public class InprogressViewHolder extends RecyclerView.ViewHolder {
        TextView workerid,date,empdate,name,skill,location,startdate,enddate,checkavailable;
        Button pay,paymenthistory,extend;
        ImageView call,profileimage;
        public InprogressViewHolder(@NonNull View itemView) {
            super(itemView);
            workerid=itemView.findViewById(R.id.Bookingid);
            date=itemView.findViewById(R.id.startdate);
            empdate=itemView.findViewById(R.id.enddate);
            name = itemView.findViewById(R.id.firstname);
            skill = itemView.findViewById(R.id.skill);
            location = itemView.findViewById(R.id.location);
            startdate = itemView.findViewById(R.id.startdate);
            enddate = itemView.findViewById(R.id.enddate);
            checkavailable= itemView.findViewById(R.id.checkavailable);
            pay = itemView.findViewById(R.id.pay);
            paymenthistory = itemView.findViewById(R.id.paymenthistory);
            extend = itemView.findViewById(R.id.extend);
            call = itemView.findViewById(R.id.call);
              profileimage = itemView.findViewById(R.id.profileimage);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            workerid.setTypeface(font);
            date.setTypeface(font);
            empdate.setTypeface(font);
            name.setTypeface(font);
            skill.setTypeface(font);
            location.setTypeface(font);
            startdate.setTypeface(font);
            enddate.setTypeface(font);
            paymenthistory.setTypeface(font);
            pay.setTypeface(font);
            extend.setTypeface(font);
        }
    }
}
