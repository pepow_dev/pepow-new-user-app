package com.example.user.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.user.Activity.Profile_Detail;
import com.example.user.Activity.TagsActivity;
import com.example.user.FavDB.DatabaseHelper;
import com.example.user.R;
import com.example.user.Response.Fave_data;
import com.example.user.model.Fav_Model;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Fave_Adapter extends RecyclerView.Adapter<Fave_Adapter.MyViewHolder> {
    private LayoutInflater inflater;
    private List<Fave_data> favlist;
    Context ctx;
    String mobilenumber;
    int MY_PERMISSIONS_REQUEST_CALL_PHONE=101;
OnItemClickListener mListener;


    public interface OnItemClickListener{
        void onCheckAvailClick(int position);
        void onShorlistItemClick(int position);
        void onremoveFaverateClick(int position);
        void  onPlaceOrderClick(int position);
        void onCalnderpicker(int position);

    }
    public void setOnITemClickListener(OnItemClickListener listener){
        mListener = listener;
    }


    public Fave_Adapter( Context ctx,List<Fave_data> favlist) {
        this.inflater = inflater.from(ctx);
        this.favlist = favlist;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fav_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
       holder.name.setText(favlist.get(position).getFirst_name());
        holder.area.setText(favlist.get(position).getArea());
        String pskill = favlist.get(position).getRegister_for();
        holder.skill.setText(pskill);
        holder.visitcharge.setText(favlist.get(position).getRate_per_day());
        holder.price.setText(favlist.get(position).getExpected_salary());
        holder.lname.setText(favlist.get(position).getLast_name());
       // holder.distance.setText(String.valueOf(fav_model.getDistance()));
        mobilenumber=favlist.get(position).getPhone_no();


        String phone = favlist.get(position).getPhone_no();
        Glide.with(ctx).load(favlist.get(position).getProfile_pic()).into(holder.prfimage);
        holder.prfimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profile = new Intent(ctx, Profile_Detail.class);
                profile.putExtra("imgurl",favlist.get(position).getProfile_pic());
                ctx.startActivity(profile);
            }
        });

        holder.callicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = ctx.getSharedPreferences("Callshared",MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Clicked","call");
                editor.apply();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+mobilenumber));

                if (ActivityCompat.checkSelfPermission(ctx,android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) ctx,
                            android.Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions((Activity) ctx,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);
                    }
                }
                ctx.startActivity(callIntent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return favlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, skill, area, visitcharge,price,lname,distance,checkavailability;
        public ImageView prfimage,redfav,callicon,calender;
        Button placeorder,shortlist;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.firstname);
            skill=itemView.findViewById(R.id.userid);
            area = itemView.findViewById(R.id.area);
           visitcharge=itemView.findViewById(R.id.visitingcharge);
           price=itemView.findViewById(R.id.rateperday);
           lname=itemView.findViewById(R.id.lastname);
           prfimage=itemView.findViewById(R.id.profileimage);
           redfav = itemView.findViewById(R.id.redfaverate);
           distance=itemView.findViewById(R.id.distance);
            callicon=itemView.findViewById(R.id.call);
            checkavailability=itemView.findViewById(R.id.checkavailable);
            placeorder = itemView.findViewById(R.id.placeorder);
            shortlist = itemView.findViewById(R.id.shortlist);
            calender =itemView.findViewById(R.id.calender);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            name.setTypeface(font);
            skill.setTypeface(font);
            area.setTypeface(font);
            visitcharge.setTypeface(font);
            price.setTypeface(font);
            lname.setTypeface(font);
            distance.setTypeface(font);
            checkavailability.setTypeface(font);
            placeorder.setTypeface(font);
            shortlist.setTypeface(font);


            calender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onCalnderpicker(position);
                        }
                    }
                }
            });
            checkavailability.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onCheckAvailClick(position);
                        }
                    }
                }
            });
            shortlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onShorlistItemClick(position);
                        }
                    }
                }
            });
            placeorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onPlaceOrderClick(position);
                        }
                    }
                }
            });

            redfav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onremoveFaverateClick(position);
                        }
                    }

                }
            });
        }
    }
}
