package com.example.user.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.user.R;
import com.example.user.model.Emp_Sch_Data_Orders;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Schedule_List_Adapter extends RecyclerView.Adapter<Schedule_List_Adapter.ScheduleVIewHolder> {
    private LayoutInflater inflater;
    private List<Emp_Sch_Data_Orders> emp_sch_data;

    Context ctx;
    OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onCancelClick(int position);
        void onClickAvailable(int position);
        void onCallClick(int position);
        void onRescheduleClick(int position);

    }
    public void setOnITemClickListener(OnItemClickListener listener){
        mListener = listener;
    }


    public Schedule_List_Adapter(List<Emp_Sch_Data_Orders> emp_sch_data, Context ctx) {
        this.inflater = inflater.from(ctx);
        this.emp_sch_data = emp_sch_data;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ScheduleVIewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.sch_inp_list, parent, false);
        ScheduleVIewHolder holder = new ScheduleVIewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleVIewHolder holder, int position) {
   holder.bookingid.setText(emp_sch_data.get(position).getOrder_no());
   holder.name.setText(emp_sch_data.get(position).getName());
   holder.date.setText(emp_sch_data.get(position).getOrder_date());
   holder.empdate.setText(emp_sch_data.get(position).getOrder_date());
        holder.skill.setText(emp_sch_data.get(position).getPrimary_skill()+","+emp_sch_data.get(position).getSecondary_skill());
         holder.location.setText(emp_sch_data.get(position).getWork_location());
         holder.Bookingstatus.setText(emp_sch_data.get(position).getStatus());
          Picasso.with(ctx).load(emp_sch_data.get(position).getProfile_pic()).into(holder.profile);

    }

    @Override
    public int getItemCount() {
        return emp_sch_data.size();
    }

    public class ScheduleVIewHolder extends RecyclerView.ViewHolder {
        TextView  bookingid,date,empdate,name,skill,location,Bookingstatus,checkavailable;
        Button cancel,reschedule;
        ImageView call,profile;
        public ScheduleVIewHolder(@NonNull View itemView) {
            super(itemView);
              bookingid=itemView.findViewById(R.id.Bookingid);
              date=itemView.findViewById(R.id.startdate);
              empdate=itemView.findViewById(R.id.startdate);
            cancel = itemView.findViewById(R.id.cancel);
             name = itemView.findViewById(R.id.name);
             skill = itemView.findViewById(R.id.skill);
             location = itemView.findViewById(R.id.location);
             Bookingstatus = itemView.findViewById(R.id.Bookingstatus);
            profile = itemView.findViewById(R.id.profileimage);
            call =itemView.findViewById(R.id.call);
            reschedule =itemView.findViewById(R.id.reschedule);
              checkavailable = itemView.findViewById(R.id.checkavailable);

              checkavailable.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      if (mListener !=null){
                          int position = getAdapterPosition();
                          if (position!= RecyclerView.NO_POSITION){
                              mListener.onClickAvailable(position);
                          }
                      }
                  }
              });
cancel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (mListener !=null){
            int position = getAdapterPosition();
            if (position!= RecyclerView.NO_POSITION){
                mListener.onCancelClick(position);
            }
        }
    }
});
reschedule.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (mListener !=null){
            int position = getAdapterPosition();
            if (position!= RecyclerView.NO_POSITION){
                mListener.onRescheduleClick(position);
            }
        }
    }
});

            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            bookingid.setTypeface(font);
            date.setTypeface(font);
            empdate.setTypeface(font);
            cancel.setTypeface(font);
            name.setTypeface(font);
            skill.setTypeface(font);
            location.setTypeface(font);
            Bookingstatus.setTypeface(font);
            reschedule.setTypeface(font);
            checkavailable.setTypeface(font);
        }
    }
}
