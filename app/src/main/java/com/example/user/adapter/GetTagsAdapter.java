package com.example.user.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.user.Activity.TagsActivity;
import com.example.user.R;
import com.example.user.Response.TagsGet_data;

import java.util.List;

public class GetTagsAdapter extends RecyclerView.Adapter<GetTagsAdapter.TagsViewHolder> {

    private LayoutInflater inflater;
    private List<TagsGet_data> tags_repos;
    private OnItemClickListener mListener;
    Context ctx;
       public interface OnItemClickListener{
           void onItemClick(int position);
       }
       public void setOnITemClickListener(OnItemClickListener listener){
           mListener = listener;
       }

    public GetTagsAdapter(Context ctx,List<TagsGet_data> tags_repos) {
        this.inflater = inflater.from(ctx);
        this.tags_repos = tags_repos;
        this.ctx = ctx;
    }


    @NonNull
    @Override
    public TagsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.tagslayout, parent, false);
        TagsViewHolder holder = new TagsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TagsViewHolder holder, final int position) {
        holder.tagname.setText(tags_repos.get(position).getTag());


    }

    @Override
    public int getItemCount() {
        return tags_repos.size();
    }


    public class TagsViewHolder extends RecyclerView.ViewHolder  {
        TextView tagname;

        public TagsViewHolder(@NonNull View itemView) {
            super(itemView);
            tagname=itemView.findViewById(R.id.tagname);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            tagname.setTypeface(font);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

        }


    }

}
