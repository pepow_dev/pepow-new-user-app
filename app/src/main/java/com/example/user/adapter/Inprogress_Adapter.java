package com.example.user.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.user.R;
import com.example.user.Response.Emp_Sch_Data;
import com.example.user.Response.Track_Imp_DataOrders;
import com.example.user.Response.Track_Inp_Data;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Inprogress_Adapter extends RecyclerView.Adapter<Inprogress_Adapter.InprogressViewHolder> {

    private LayoutInflater inflater;
    private List<Track_Inp_Data> emp_sch_data;
    Context ctx;
    OnItemClickListener mListener;
    Inprogress_List_Adapter inprogress_list_adapter;
    public interface OnItemClickListener{
        void onViewClick(int position);
        void onClickAvailable(int position);
        void onCallClick(int position);
        void onExtendClick(int position);
        void onPaymentHistoryClick(int position);
    }
    public void setOnITemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public Inprogress_Adapter(Context ctx,List<Track_Inp_Data> emp_sch_data) {
        this.inflater = inflater.from(ctx);
        this.emp_sch_data = emp_sch_data;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public Inprogress_Adapter.InprogressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.tracking_inprogress_expans, parent, false);
        InprogressViewHolder holder = new InprogressViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Inprogress_Adapter.InprogressViewHolder holder, int position) {

            String tag = emp_sch_data.get(position).getTag_name();
            holder.tagname.setText(tag);
            Log.d("tag",tag);

             //   holder.workerid.setText("#"+emp_sch_data.get(position).getOrders().get(0).getOrder_no());
             //   holder.date.setText(emp_sch_data.get(position).getOrders().get(0).getOrder_date());
           //    holder.empdate.setText(emp_sch_data.get(position).getOrders().get(0).getOrder_start_date());
            //   holder.name.setText(emp_sch_data.get(position).getOrders().get(0).getName());
             //   holder.skill.setText(emp_sch_data.get(position).getOrders().get(0).getPrimary_skill()+","+emp_sch_data.get(position).getOrders().get(0).getSecondary_skill());
              //  holder.location.setText(emp_sch_data.get(position).getOrders().get(0).getWork_location());
              //  Picasso.with(ctx).load(emp_sch_data.get(position).getOrders().get(0).getProfile_pic()).into(holder.profileimage);
        inprogress_list_adapter= new Inprogress_List_Adapter(emp_sch_data.get(position).getOrders(),ctx);
       holder.tracklist.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        holder.tracklist.setHasFixedSize(true);
        holder.tracklist.setAdapter(inprogress_list_adapter);
//      notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return emp_sch_data.size();
    }

    public class InprogressViewHolder extends RecyclerView.ViewHolder {
        RecyclerView tracklist;
        TextView tagname;
      //  Button pay,paymenthistory,extend;
      //  ImageView call,profileimage;
        public InprogressViewHolder(@NonNull View itemView) {
            super(itemView);
            tracklist=itemView.findViewById(R.id.trackinplist);
          //  workerid=itemView.findViewById(R.id.Bookingid);
           // date=itemView.findViewById(R.id.startdate);
          ////  empdate=itemView.findViewById(R.id.enddate);
          //  name = itemView.findViewById(R.id.firstname);
          //  skill = itemView.findViewById(R.id.skill);
          //  location = itemView.findViewById(R.id.location);
           // startdate = itemView.findViewById(R.id.startdate);
          //  enddate = itemView.findViewById(R.id.enddate);
         //   checkavailable= itemView.findViewById(R.id.checkavailable);
            tagname=itemView.findViewById(R.id.tagname);
         //   pay = itemView.findViewById(R.id.pay);
          //  paymenthistory = itemView.findViewById(R.id.paymenthistory);
           // extend = itemView.findViewById(R.id.extend);

           // call = itemView.findViewById(R.id.call);
          //  profileimage = itemView.findViewById(R.id.profileimage);

           /* paymenthistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onPaymentHistoryClick(position);
                        }
                    }
                }
            });

            extend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onExtendClick(position);
                        }
                    }
                }
            });
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onCallClick(position);
                        }
                    }
                }
            });
            checkavailable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onClickAvailable(position);
                        }
                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onViewClick(position);
                        }
                    }
                }
            });
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            workerid.setTypeface(font);
            date.setTypeface(font);
            empdate.setTypeface(font);
            name.setTypeface(font);
             skill.setTypeface(font);
            location.setTypeface(font);
            startdate.setTypeface(font);
            enddate.setTypeface(font);
            paymenthistory.setTypeface(font);
            pay.setTypeface(font);
            extend.setTypeface(font);*/


        }
    }
}
