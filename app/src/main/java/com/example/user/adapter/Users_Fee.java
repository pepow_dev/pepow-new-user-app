package com.example.user.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.user.Activity.Profile_Detail;
import com.example.user.Activity.TagsActivity;
import com.example.user.FavDB.DatabaseHelper;
import com.example.user.HomeActivity;
import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.R;
import com.example.user.Response.DataList;
import com.example.user.Response.ShortlistRepo;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class Users_Fee extends RecyclerView.Adapter<Users_Fee.UsersViewHolder> {
    private LayoutInflater inflater;
    private List<DataList> modelRecyclerArrayList;
    OnItemClickListener mListener;
    Context ctx;

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onShorlistItemClick(int position);
        void onAddFaverateClick(int position);
        void onRemoveFaverateClick(int position);
        void onCalnderpicker(int position);
        void onavailable(int position);
        void onrateclick(int position);
    }
    public void setOnITemClickListener(OnItemClickListener listener){
        mListener = listener;
    }


    int MY_PERMISSIONS_REQUEST_CALL_PHONE=101;
    String workerid,area,primaryskill,mobilenmbr,visitingcharg,ratperday,fname,lname,imgurl,secondaryskill;
    float dist;
    DatabaseHelper db;
    public Users_Fee(Context ctx, List<DataList> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.users_fee, viewGroup, false);
        UsersViewHolder holder = new UsersViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final UsersViewHolder holder, final int position) {
        holder.firstname.setText(modelRecyclerArrayList.get(position).getFirst_name());
        holder.lastname.setText(modelRecyclerArrayList.get(position).getLast_name());
        area = modelRecyclerArrayList.get(position).getWork_location();
        primaryskill = modelRecyclerArrayList.get(position).getPrimary_skill();
        imgurl=modelRecyclerArrayList.get(position).getProfile_pic();
        secondaryskill = modelRecyclerArrayList.get(position).getSecondary_skill();
        mobilenmbr = modelRecyclerArrayList.get(position).getPhone_no();
        visitingcharg = modelRecyclerArrayList.get(position).getExpected_salary();
        ratperday = modelRecyclerArrayList.get(position).getRate_per_day();

        holder.rateperday.setText(ratperday+" /day or hr");
        holder.visitingcharge.setText("Visit charge:"+visitingcharg);
        holder.userid.setText(primaryskill + "," + secondaryskill);
        Glide.with(ctx).load(modelRecyclerArrayList.get(position).getProfile_pic()).into(holder.profileimage);
        SharedPreferences sharedPreferences1 = ctx.getSharedPreferences("sharedlocation", MODE_PRIVATE);
        String ll = sharedPreferences1.getString("location", null);
        String lat = sharedPreferences1.getString("lat", null);
        String longi = sharedPreferences1.getString("longi", null);
        Location location1 = new Location("");
        location1.setLatitude(Double.parseDouble(lat));
        location1.setLongitude(Double.parseDouble(longi));

        Location location = new Location("");
        location.setLatitude(Double.parseDouble(lat));
        location.setLongitude(Double.parseDouble(longi));
         dist = location1.distanceTo(location);
        holder.distance.setText(dist+"km");
        holder.area.setText(area+",");



        holder.callicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = ctx.getSharedPreferences("Callshared",MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Clicked","call");
                editor.apply();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+mobilenmbr));

                if (ActivityCompat.checkSelfPermission(ctx,android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) ctx,
                            android.Manifest.permission.CALL_PHONE)) {
                    } else {
                        ActivityCompat.requestPermissions((Activity) ctx,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);
                    }
                }
                ctx.startActivity(callIntent);
            }
        });





        holder.profileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profile = new Intent(ctx, Profile_Detail.class);
                profile.putExtra("imgurl",modelRecyclerArrayList.get(position).getProfile_pic());
                ctx.startActivity(profile);
            }
        });

        String faveratestatus = modelRecyclerArrayList.get(position).getFavourite();
        if (faveratestatus.equals("0")){
            holder.favicon.setVisibility(View.VISIBLE);
            holder.refav.setVisibility(View.GONE);
        }else if (faveratestatus.equals("1")){
            holder.favicon.setVisibility(View.GONE);
            holder.refav.setVisibility(View.VISIBLE);
        }
    }




    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class UsersViewHolder extends RecyclerView.ViewHolder {
        TextView firstname,lastname,userid,checkavailability,area,distance,visitingcharge,rateperday;
        ImageView profileimage,callicon,favicon,refav,calender;
        Button placeorder,shortlist,rate;
        public UsersViewHolder(@NonNull  View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"font/Uber Move Text.ttf");
            firstname=itemView.findViewById(R.id.firstname);
            lastname=itemView.findViewById(R.id.lastname);
            profileimage=itemView.findViewById(R.id.profileimage);
            userid=itemView.findViewById(R.id.userid);
            area = itemView.findViewById(R.id.area);
            distance=itemView.findViewById(R.id.distance);
            checkavailability=itemView.findViewById(R.id.checkavailable);
            callicon=itemView.findViewById(R.id.call);
            visitingcharge=itemView.findViewById(R.id.visitingcharge);
            rateperday=itemView.findViewById(R.id.rateperday);
            favicon=itemView.findViewById(R.id.faverate);
            refav=itemView.findViewById(R.id.redfaverate);
            placeorder = itemView.findViewById(R.id.placeorder);
            shortlist = itemView.findViewById(R.id.shortlist);
            calender=itemView.findViewById(R.id.calender);
            rate=itemView.findViewById(R.id.rate);
            rate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onrateclick(position);
                        }
                    }
                }
            });
            checkavailability.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onavailable(position);
                        }
                    }
                }
            });
           calender.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   if (mListener !=null){
                       int position = getAdapterPosition();
                       if (position!= RecyclerView.NO_POSITION){
                           mListener.onCalnderpicker(position);
                       }
                   }
               }
           });

            favicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onAddFaverateClick(position);
                        }
                    }
                }
            });
            refav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onRemoveFaverateClick(position);
                        }
                    }
                }
            });

            placeorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
            shortlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener !=null){
                        int position = getAdapterPosition();
                        if (position!= RecyclerView.NO_POSITION){
                            mListener.onShorlistItemClick(position);
                        }
                    }
                }
            });
                        checkavailability.setTypeface(font);
                        firstname.setTypeface(font);
                        lastname.setTypeface(font);
                        userid.setTypeface(font);
                        area.setTypeface(font);
                        distance.setTypeface(font);

                    }
                }
            }
