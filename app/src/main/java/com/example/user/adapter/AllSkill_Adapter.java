package com.example.user.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.user.Activity.Skills;
import com.example.user.R;
import com.example.user.Response.All_Skill;
import com.example.user.Response.Data_AllSkill;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class AllSkill_Adapter extends RecyclerView.Adapter<AllSkill_Adapter.AllSkillViewHolder> {
    private LayoutInflater inflater;
    private List<Data_AllSkill> modelRecyclerArrayList1;
    Context ctx;
    public AllSkill_Adapter(Context ctx,List<Data_AllSkill> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList1 = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public AllSkillViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.all_skills, parent, false);
        AllSkillViewHolder holder = new AllSkillViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AllSkillViewHolder holder, final int position) {
        holder.firstname.setText(modelRecyclerArrayList1.get(position).getName());
        Glide.with(ctx).load(modelRecyclerArrayList1.get(position).getImage()).into(holder.skillimage);
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ctx, Skills.class);
                            SharedPreferences sharedPreferences = ctx.getSharedPreferences("Skillsname",MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("skillname",holder.firstname.getText().toString());
                            editor.apply();
                            ctx.startActivity(intent);


                        }
                    });
    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList1.size();
    }

    public class AllSkillViewHolder extends RecyclerView.ViewHolder {
        TextView firstname;
        ImageView skillimage;
        public AllSkillViewHolder(@NonNull View itemView) {
            super(itemView);
            firstname=itemView.findViewById(R.id.skill_name);
            skillimage = itemView.findViewById(R.id.skillimage);
        }
    }
}
