package com.example.user;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.user.Activity.Employment;
import com.example.user.Activity.Faverate;
import com.example.user.Activity.LoginScreen;
import com.example.user.Activity.Shortlist;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
   public Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    private static final String USER_PREFS = "SeekingDaddie";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        toolbar.setTitleTextColor(Color.BLACK);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setBackgroundResource(R.color.themecolourblue);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) this,
                    android.Manifest.permission.READ_PHONE_STATE)) {
            } else {
                ActivityCompat.requestPermissions((Activity) this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        1);
            }
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
        {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dashboard) {
               Intent home = new Intent(MainActivity.this,HomeActivity.class);
               startActivity(home);
               finish();
           drawer.closeDrawers();
            // Handle the camera action
        } else if (id == R.id.faverate) {
              Intent fave = new Intent(MainActivity.this, Faverate.class);
              startActivity(fave);
              finish();
           drawer.closeDrawers();
        }else if (id == R.id.shortlist) {
            Intent shortlist = new Intent(MainActivity.this, Shortlist.class);
            startActivity(shortlist);
            finish();
            drawer.closeDrawers();
        }else if (id == R.id.employment) {
            Intent shortlist = new Intent(MainActivity.this, Employment.class);
            startActivity(shortlist);
            finish();
            drawer.closeDrawers();
        }
        else if (id==R.id.logout)
        {
            SharedPreferences sharedpreferences = getSharedPreferences(LoginScreen.USER_PREFS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear();
            editor.commit();
            this.appSharedPrefs = MainActivity.this.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
            appSharedPrefs.getString("user_id",null);
            this.prefsEditor = appSharedPrefs.edit();
            prefsEditor.clear();
            prefsEditor.apply();
            Intent intent = new Intent(MainActivity.this, LoginScreen.class);
            startActivity(intent);
            finish();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
