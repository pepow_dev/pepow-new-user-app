package com.example.user.FavDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.w3c.dom.Text;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "FavDB";
    private static final String TABLE_NAME = "newTable";
    private static final String KEY_ID = "USERID";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME
                + "( NAME STRING, SKILL STRING, AREA STRING, PHONE STRING, VISITCHARGE STRING, PRICE STRING, LNAME STRING, IMAGE STRING, SECONDSKILL STRING, USERID TEXT, DISTANCE FLOAT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void insertMsg(String name,String skill,String area,String phone,String visitcharge,String price,String lname,String image,String secondskill,String userid,float distance) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("NAME", name);
        cv.put("SKILL",skill);
        cv.put("AREA",area);
        cv.put("PHONE",phone);
        cv.put("VISITCHARGE",visitcharge);
        cv.put("PRICE",price);
        cv.put("LNAME",lname);
        cv.put("IMAGE",image);
        cv.put("SECONDSKILL",secondskill);
        cv.put("USERID",userid);
        cv.put("DISTANCE",distance);

        db.insert(TABLE_NAME, null, cv);

        db.close();
    }

    public Cursor viewData() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor ;
        String query = "Select * from " +TABLE_NAME;
        cursor= sqLiteDatabase.rawQuery(query, null);
        return cursor;
    }

    public boolean deleteTitle(String name)
    {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_NAME, "NAME" + "=?", new String[]{name}) > 0;
    }
    public boolean checkIfMyTitleExists(String name) {
        SQLiteDatabase db = getWritableDatabase();
        String Query = "Select * from " + TABLE_NAME + " where " + "NAME" + " = " + "'" + name + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }

}
