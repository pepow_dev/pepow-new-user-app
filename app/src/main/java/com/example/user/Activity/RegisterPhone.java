package com.example.user.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterPhone extends AppCompatActivity {
    Button button;
    private EditText et_num;
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;
    String phone_no;
    public static String NEW_OTP_URL= "http://ujalvomigroup.com/pepow/api/user_otp_validate.php?type=user";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_phone);
        button=findViewById(R.id.submit);
        et_num = findViewById(R.id.etnum);
        internetConnection = new InternetConnection(this);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verification();
                sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("phone",et_num.getText().toString());
                editor.apply();
            }
        });
    }

    private void verification() {
        String phoneNumber = "";
        if (et_num.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Enter your mobile no.", Snackbar.LENGTH_LONG).show();
            et_num.requestFocus();
            et_num.setCursorVisible(true);

        } else if (et_num.length() < 10) {
            Snackbar.make(findViewById(android.R.id.content), "Phone number should be 10 digits", Snackbar.LENGTH_LONG).show();
            et_num.requestFocus();
            et_num.setCursorVisible(true);
        } else if (phoneNumber.matches("^[7-9][0-9]{9}$")) {
            Snackbar.make(findViewById(android.R.id.content), "Invalid number ", Snackbar.LENGTH_LONG).show();
        } else { if (internetConnection.isConnectingToInternet()) {
            next();
            Snackbar.make(findViewById(android.R.id.content), "Wait..", Snackbar.LENGTH_LONG).show();

            } else {
            Snackbar.make(findViewById(android.R.id.content), "Please Check Internet Connectivity", Snackbar.LENGTH_LONG).show();
            }
        }
    }
    private void next() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,NEW_OTP_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    Toast.makeText(getApplicationContext(), "1111  ",Toast.LENGTH_LONG).show();
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    Toast.makeText(getApplicationContext(), message + "  ",Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("success")) {

                        phone_no = jsonObject.getString("phone_no");
                        String s1 = et_num.getText().toString();
                        Intent intent = new Intent(RegisterPhone.this, OTPVerifie_Screen.class);
                        intent.putExtra("9876543212", s1);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Snackbar.make(findViewById(android.R.id.content), response, Snackbar.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterPhone.this, error.getMessage(), Toast.LENGTH_LONG).show();
                Log.d("onerror",error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phone_no", et_num.getText().toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(RegisterPhone.this);
        requestQueue.add(stringRequest);
    }
}
