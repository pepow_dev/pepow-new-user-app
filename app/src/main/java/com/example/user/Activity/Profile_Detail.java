package com.example.user.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.HomeActivity;
import com.example.user.R;
import com.mikhaellopez.circularimageview.CircularImageView;

public class Profile_Detail extends AppCompatActivity {
    Toolbar toolbar;
    TextView name,veradhar,veribank,address,skill,wrkexp,avilable,vstchrge,daily,feedback;
    CircularImageView imageView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile__detail);
        toolbar = findViewById(R.id.toolbar);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        toolbar.setNavigationIcon(R.drawable.arrowback);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        name=findViewById(R.id.name);
        veradhar=findViewById(R.id.adharveri);
        veribank=findViewById(R.id.bankveri);
        address=findViewById(R.id.address);
        skill=findViewById(R.id.skill);
        wrkexp=findViewById(R.id.workexp);
        avilable=findViewById(R.id.availtext);
        vstchrge=findViewById(R.id.visicharge);
        daily=findViewById(R.id.hourly);
        feedback=findViewById(R.id.feedback);
        imageView=findViewById(R.id.imageprofile);
        Typeface font = Typeface.createFromAsset(getAssets(),"font/Uber Move Text.ttf");
        name.setTypeface(font);
        veradhar.setTypeface(font);
        veribank.setTypeface(font);
        address.setTypeface(font);
        skill.setTypeface(font);
        wrkexp.setTypeface(font);
        avilable.setTypeface(font);
        vstchrge.setTypeface(font);
        daily.setTypeface(font);
        feedback.setTypeface(font);
        Intent intent = getIntent();
        String img = intent.getStringExtra("imgurl");
        Glide.with(this).load(img).into(imageView);


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
