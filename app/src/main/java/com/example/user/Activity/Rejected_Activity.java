package com.example.user.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import com.example.user.R;

public class Rejected_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_);
    }

    public void okay(View view) {
        Intent intent = new Intent(Rejected_Activity.this, RegisterPhone.class);
        startActivity(intent);
        finish();
    }
}
