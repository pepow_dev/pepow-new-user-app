package com.example.user.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.user.MainActivity;
import com.example.user.R;
import com.example.user.pager.Employ_ViewPager;
import com.google.android.material.tabs.TabLayout;

public class Employment extends MainActivity {
    FrameLayout contentFrameLayout;
    Button button;
    ProgressBar progressBar;
    RelativeLayout cartrelative;
    TabLayout tabLayout;
    ViewPager viewPager;
   Employ_ViewPager employ_viewPager;



    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_employment,contentFrameLayout);
        toolbar.setTitle("Tracking");
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getColor(R.color.white));
        toolbar.setTitleTextAppearance(Employment.this, R.style.PagerTabStripText1);
        button = toolbar.findViewById(R.id.button);
        progressBar= toolbar.findViewById(R.id.toolbar_progress_bar);
        cartrelative=toolbar.findViewById(R.id.cartrelative);
        button.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        cartrelative.setVisibility(View.GONE);
        tabLayout=findViewById(R.id.tabLayout2);
        viewPager=findViewById(R.id.viewpager);
        employ_viewPager=new Employ_ViewPager(getSupportFragmentManager());
        viewPager.setAdapter(employ_viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }
}
