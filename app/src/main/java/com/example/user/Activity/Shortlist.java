package com.example.user.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.MainActivity;
import com.example.user.R;
import com.example.user.Response.AddFave_Repo;
import com.example.user.Response.ConfirmOrder_Repo;
import com.example.user.Response.ConfirmOrder_data;
import com.example.user.Response.RemoveFave_Repo;
import com.example.user.Response.Remove_Shortlist_Repo;
import com.example.user.Response.ShortgetList_Data;
import com.example.user.Response.ShortgetList_Repo;
import com.example.user.Response.TagsGet_Repo;
import com.example.user.Response.TagsGet_data;
import com.example.user.adapter.Shortlist_Adapter;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Shortlist extends MainActivity {
    FrameLayout contentFrameLayout;
    Button button;
    ProgressBar progressBar;
    RelativeLayout cartrelative;
    private Shortlist_Adapter shortlist_adapter;
    List<ShortgetList_Data> dataLists;
    RecyclerView recyclerView;
    FloatingActionButton submit;
    SharedPreferenceClass sharedPreferenceClass;
    String userid,workerid,selecteddate,wrkid,selected,on,today;
    JsonObject jsonObject;
    ProgressDialog progressDialog;
    TextView emtyrecord;
    int MY_PERMISSIONS_REQUEST_CALL_PHONE=101;
    List<TagsGet_data> dataLists1;
    List<String> taglist;
    List<String> tagid;
    ArrayAdapter<String> arrayAdapter;
    Spinner spinner;
    Integer nmmm;
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_shortlist,contentFrameLayout);
        toolbar.setTitle("Short list");
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getColor(R.color.white));
        toolbar.setTitleTextAppearance(Shortlist.this, R.style.PagerTabStripText1);
        button = toolbar.findViewById(R.id.button);
        progressBar= toolbar.findViewById(R.id.toolbar_progress_bar);
        cartrelative=toolbar.findViewById(R.id.cartrelative);
        emtyrecord=findViewById(R.id.emtyrecord);
        button.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        cartrelative.setVisibility(View.GONE);
        recyclerView=findViewById(R.id.shortrecycle);
        submit=findViewById(R.id.floatingsubmit);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateandTime = sdf.format(new Date());

        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        today = dayFormat.format(new Date());
        nmmm=  sdf.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
     //   new ItemTouchHelper(itemtouchhelper).attachToRecyclerView(recyclerView);
        sharedPreferenceClass = new SharedPreferenceClass(Shortlist.this);
        userid = sharedPreferenceClass.getValue_string("user_id");
        progressDialog=new ProgressDialog(Shortlist.this);
        taglist=new ArrayList<>();
        tagid=new ArrayList<>();
        UsersDetails();
        getTags();

    }


    private void UsersDetails() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<ShortgetList_Repo> call = jsonpost.getshortlist(data);
        call.enqueue(new Callback<ShortgetList_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<ShortgetList_Repo> call, retrofit2.Response<ShortgetList_Repo> response) {
                if (response.isSuccessful()){
                    Toast.makeText(Shortlist.this, "success", Toast.LENGTH_SHORT).show();
                    assert response.body() != null;
                   dataLists = response.body().getData();
                    if (dataLists.size()==0){
                        emtyrecord.setVisibility(View.VISIBLE);
                    }else {
                        emtyrecord.setVisibility(View.GONE);
                    }
                    shortlist_adapter=new Shortlist_Adapter(Shortlist.this,dataLists);

                    recyclerView.setLayoutManager(new LinearLayoutManager(Shortlist.this, LinearLayoutManager.VERTICAL, false));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(shortlist_adapter);
                    shortlist_adapter.setOnITemClickListener(new Shortlist_Adapter.OnItemClickListener() {
                        @Override
                        public void ondeleteClick(final int position) {
                            final String ss = dataLists.get(position).getWorker_id();
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Shortlist.this);
                            alertDialogBuilder.setMessage("Do You Want To Delete It");
                            alertDialogBuilder
                                    .setCancelable(false)
                                    .setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                 removeItem(ss);
                                                    progressDialog.setMessage("Wait...");
                                                    progressDialog.show();
                                                }
                                            })
                                    .setNegativeButton("Cancel",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });


                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                        @Override
                        public void onplaceorderClick(final int position) {
                            wrkid = dataLists.get(position).getWorker_id();
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(Shortlist.this);
                            final View dialogView =getLayoutInflater().inflate(R.layout.confirmorder_dialog, null);
                            dialog.setView(dialogView);
                            dialog.setCancelable(false);
                            Button dialog_button = (Button) dialogView.findViewById(R.id.addtag);
                            spinner = dialogView.findViewById(R.id.days);
                            spinner.setAdapter(arrayAdapter);
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    selected = tagid.get(i);
                                    Toast.makeText(Shortlist.this, selected, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                            Button textView1 = dialogView.findViewById(R.id.dialog_button);
                            final TextView textView = dialogView.findViewById(R.id.seletedduration);
                            final Button one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,
                                    eighteen,ninteen,twenty,twentyone,twentytwo;
                            one=dialogView.findViewById(R.id.one);
                            one.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= one.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            two=dialogView.findViewById(R.id.two);
                            two.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= two.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            three=dialogView.findViewById(R.id.three);
                            three.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= three.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            four=dialogView.findViewById(R.id.four);
                            four.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= four.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            five=dialogView.findViewById(R.id.five);
                            five.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= five.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            six=dialogView.findViewById(R.id.six);
                            six.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= six.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            seven=dialogView.findViewById(R.id.seven);
                            seven.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= seven.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            eight=dialogView.findViewById(R.id.eight);
                            eight.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= eight.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            nine=dialogView.findViewById(R.id.nine);
                            nine.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= nine.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            ten=dialogView.findViewById(R.id.ten);
                            ten.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= ten.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            eleven=dialogView.findViewById(R.id.eleven);
                            eleven.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= eleven.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            twelve=dialogView.findViewById(R.id.twelve);
                            twelve.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= twelve.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            thirteen=dialogView.findViewById(R.id.thirteen);
                            thirteen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= thirteen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            fourteen=dialogView.findViewById(R.id.fourteen);
                            fourteen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= fourteen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            fifteen=dialogView.findViewById(R.id.fifteen);
                            fifteen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= fifteen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            sixteen=dialogView.findViewById(R.id.sixteen);
                            sixteen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= sixteen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            seventeen=dialogView.findViewById(R.id.seventeen);
                            seventeen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= seventeen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            eighteen=dialogView.findViewById(R.id.eighteen);
                            eighteen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= eighteen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            ninteen=dialogView.findViewById(R.id.ninteen);
                            ninteen.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= ninteen.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            twenty=dialogView.findViewById(R.id.twenty);
                            twenty.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= twenty.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            twentyone=dialogView.findViewById(R.id.twentyone);
                            twentyone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= twentyone.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            twentytwo=dialogView.findViewById(R.id.twentytwo);
                            twentytwo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    on= twentytwo.getText().toString();
                                    textView.setText("Selected "+on);
                                }
                            });
                            final String nmb = textView.getText().toString();
                            dialog_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent tagintent = new Intent(Shortlist.this, TagsActivity.class);
                                    startActivity(tagintent);
                                }
                            });
                            final AlertDialog alertDialog = dialog.create();
                            textView1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    SharedPreferences sharedPreferences =getSharedPreferences("SharedTag",MODE_PRIVATE);
                                    // selected = sharedPreferences.getString("tag",null);
                                    String wk = dataLists.get(position).getWorker_id();
                                      postConfirm(userid,wk,nmb,selected,selecteddate);
                                    alertDialog.dismiss();
                                   // progressDialog.setMessage("Wait....");
                                    //progressDialog.show();
                                  //  placeOrder(selected,wrkid);
                                       /* if (tagv!=null){
                                        }else {

                                            Toast.makeText(Skills.this, "please add tag", Toast.LENGTH_SHORT).show();
                                        }*/


                                }
                            });
                            alertDialog.show();

                        }

                        @Override
                        public void onAddFaverateClick(int position) {
                            String workerid = dataLists.get(position).getWorker_id();
                            addToFaverate(workerid);
                        }

                        @Override
                        public void onRemoveFaverateClick(int position) {
                            String workerid = dataLists.get(position).getWorker_id();
                                 removefaverate(workerid);
                        }

                        @Override
                        public void onCallButtonClick(int position) {
                            String mobilenmbr = dataLists.get(position).getPhone_no();
                            SharedPreferences sharedPreferences = getSharedPreferences("Callshared",MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("Clicked","call");
                            editor.apply();
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:"+mobilenmbr));

                            if (ActivityCompat.checkSelfPermission(Shortlist.this,android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) Shortlist.this,
                                        android.Manifest.permission.CALL_PHONE)) {
                                } else {
                                    ActivityCompat.requestPermissions((Activity) Shortlist.this,
                                            new String[]{android.Manifest.permission.CALL_PHONE},
                                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                                }
                            }
                            startActivity(callIntent);
                        }

                        @Override
                        public void onDatepickerClick(int position) {
                              datePicker();
                        }

                        @Override
                        public void oncheckavailableClick(int position) {
                             availabledate();
                        }
                    });
                    shortlist_adapter.OnItemTouch(new Shortlist_Adapter.OnItemTouchListener() {
                        @Override
                        public void OnitemTouch(int position) {
                            SharedPreferences sharedPreferences =getSharedPreferences("WorkerSHARED", Context.MODE_PRIVATE);
                            workerid=sharedPreferences.getString("workerid",null);
                            Toast.makeText(Shortlist.this, "id  "+workerid, Toast.LENGTH_SHORT).show();
                        }
                    });
                    submit.setVisibility(View.VISIBLE);
                    if (response.body().getData().size()==0){
                        recyclerView.setVisibility(View.GONE);
                        submit.setVisibility(View.GONE);
                       // nodata.setVisibility(View.VISIBLE);
                    }else {
                       // nodata.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                }else {
                    submit.setVisibility(View.GONE);
                    Toast.makeText(Shortlist.this, "else", Toast.LENGTH_SHORT).show();
                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<ShortgetList_Repo> call, Throwable t) {
                Log.d("onfail",String.valueOf(t.getMessage()));
                submit.setVisibility(View.GONE);
                Toast.makeText(Shortlist.this, "fail", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void postConfirm(String userid, String workerid, String nmb, String selected, String selecteddate) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("no_of_days",nmb);
        jsonObject.addProperty("tag_id",selected);
        jsonObject.addProperty("order_start_date",selecteddate);
        Log.d("id",userid+" "+workerid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<ConfirmOrder_Repo>call = jsonpost.confirmorder(jsonObject);
        call.enqueue(new Callback<ConfirmOrder_Repo>() {
            @Override
            public void onResponse(Call<ConfirmOrder_Repo> call, Response<ConfirmOrder_Repo> response) {
                if (response.isSuccessful()){
                    UsersDetails();
                    String msg = response.body().getMessage();
                    Log.d("fail", String.valueOf(response.errorBody()));

                    Toast.makeText(Shortlist.this, msg, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(Shortlist.this, "else", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ConfirmOrder_Repo> call, Throwable t) {
                Toast.makeText(Shortlist.this, "fail", Toast.LENGTH_SHORT).show();
                Log.d("fail",t.getMessage());

            }
        });


    }

    private void addToFaverate(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("task","add");
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<AddFave_Repo> call = jsonpost.addfaverate(jsonObject);
        call.enqueue(new Callback<AddFave_Repo>() {
            @Override
            public void onResponse(Call<AddFave_Repo> call, Response<AddFave_Repo> response) {
                if (response.isSuccessful()){
                    String s = response.body().getMessage();
                    UsersDetails();
                    shortlist_adapter.notifyDataSetChanged();
                    Toast.makeText(Shortlist.this,s, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(Shortlist.this, "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddFave_Repo> call, Throwable t) {

            }
        });
    }
    private void removefaverate(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("task","remove");
        Toast.makeText(Shortlist.this, workerid+" "+userid, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<RemoveFave_Repo> call = jsonpost.removefav(jsonObject);
        call.enqueue(new Callback<RemoveFave_Repo>() {
            @Override
            public void onResponse(Call<RemoveFave_Repo> call, Response<RemoveFave_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    String msg = response.body().getMessage();
                    UsersDetails();
                    shortlist_adapter.notifyDataSetChanged();
                    Toast.makeText(Shortlist.this, msg, Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(Shortlist.this,"else  "+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RemoveFave_Repo> call, Throwable t) {
                Toast.makeText(Shortlist.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void removeItem(String ss) {
        SharedPreferences sharedPreferences =getSharedPreferences("WorkerSHARED", Context.MODE_PRIVATE);
        workerid=sharedPreferences.getString("workerid",null);
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",ss);
        jsonObject.addProperty("task","remove");
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Remove_Shortlist_Repo> call = jsonpost.removeshortlist(jsonObject);
        call.enqueue(new Callback<Remove_Shortlist_Repo>() {
            @Override
            public void onResponse(Call<Remove_Shortlist_Repo> call, Response<Remove_Shortlist_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    String msg = response.body().getMessage();
                    UsersDetails();
                   // Toast.makeText(Shortlist.this, msg, Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Remove_Shortlist_Repo> call, Throwable t) {

            }
        });
    }
    private void datePicker() {
        // Get Calendar instance
        final Calendar calendar = Calendar.getInstance();

        // Initialize DateSetListener of DatePickerDialog
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                // Set the selected Date Info to Calendar instance
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                // Set Date Format
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

                // Set Date in input_dob EditText
                // datepick.setText(dateFormat.format(calendar.getTime()));
                selecteddate = dateFormat.format(calendar.getTime());
                // getTimeSlots(ss);


            }
        };


        // Initialize DatePickerDialog
        DatePickerDialog datePicker = new DatePickerDialog
                (
                        this,
                        date,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
        // datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        // Show datePicker Dialog
        datePicker.show();
    }
    private void getTags() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<TagsGet_Repo> call = jsonpost.gettagslist(data);
        call.enqueue(new Callback<TagsGet_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<TagsGet_Repo> call, retrofit2.Response<TagsGet_Repo> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    dataLists1 = response.body().getData();
                    for (int i=0;i<dataLists1.size();i++){
                        String tg = dataLists1.get(i).getTag();
                        String tgid = dataLists1.get(i).getId();
                        taglist.add(tg);
                        tagid.add(tgid);
                        arrayAdapter=new ArrayAdapter<String>(Shortlist.this, android.R.layout.simple_spinner_item, taglist);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                    }

                    if (response.body().getData().size()==0){

                    }else {

                    }

                }else {

                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<TagsGet_Repo> call, Throwable t) {
                //  Log.d("onfail",String.valueOf(t.getMessage()));

            }
        });
    }
    private void availabledate() {
        Dialog dialog = new Dialog(Shortlist.this);
        dialog.setContentView(R.layout.availabilitylayout);
        //firstlayer
        TextView fl1,fl2,fl3,fl4,fl5,fl6,fl7;
        //secondlayer
        TextView sl1,sl2,sl3,sl4,sl5,sl6,sl7;
        //thirdlayer
        TextView thl1,thl2,thl3,thl4,thl5,thl6,thl7;
        //fourthlayer
        TextView frl1,frl2,frl3,frl4,frl5,frl6,frl7;
        //fifthlayer
        TextView fifl1,fifl2,fifl3;
        fl1=dialog.findViewById(R.id.fl1);
        fl1.setText(today+"\n NA");
        fl2=dialog.findViewById(R.id.fl2);
        int nm = Integer.parseInt(today);
        int res = nm+1;
        if (res>nmmm){
            fl2.setText("");
        }else {
            fl2.setText(Integer.toString(res)+"\n NA");
        }

        fl3=dialog.findViewById(R.id.fl3);
        int res1 = nm+2;
        if (res1>nmmm){
            fl3.setText("");
        }else {
            fl3.setText(Integer.toString(res1)+"\n NA");
        }

        fl4=dialog.findViewById(R.id.fl4);
        int res2 = nm+3;
        if (res2>nmmm){
            fl4.setText("");
        }else {
            fl4.setText(Integer.toString(res2)+"\n NA");
        }

        fl5=dialog.findViewById(R.id.fl5);
        int res3 = nm+4;
        if (res3>nmmm){
            fl5.setText("");
        }else {
            fl5.setText(Integer.toString(res3)+"\n NA");
        }

        fl6=dialog.findViewById(R.id.fl6);
        int res4 = nm+5;
        if (res4>nmmm){
            fl6.setText("");
        }else {
            fl6.setText(Integer.toString(res4)+"\n NA");
        }

        fl7=dialog.findViewById(R.id.fl7);
        int res5 = nm+6;
        if (res5>nmmm){
            fl7.setText("");
        }else {
            fl7.setText(Integer.toString(res5)+"\n NA");
        }


        sl1=dialog.findViewById(R.id.sl1);
        int res6 = nm+7;
        if (res6>nmmm){
            sl1.setText("");
        }else {
            sl1.setText(Integer.toString(res6)+"\n NA");
        }

        sl2=dialog.findViewById(R.id.sl2);
        int res7 = nm+8;
        if (res7>nmmm){
            sl2.setText("");
        }else {
            sl2.setText(Integer.toString(res7)+"\n NA");
        }

        sl3=dialog.findViewById(R.id.sl3);
        int res8 = nm+9;
        if (res8>nmmm){
            sl3.setText("");
        }else {
            sl3.setText(Integer.toString(res8)+"\n NA");
        }

        sl4=dialog.findViewById(R.id.sl4);
        int res9 = nm+10;
        if (res9>nmmm){
            sl4.setText("");
        }else {
            sl4.setText(Integer.toString(res9)+"\n NA");
        }

        sl5=dialog.findViewById(R.id.sl5);
        int res10 = nm+11;
        if (res10>nmmm){
            sl5.setText("");
        }else {
            sl5.setText(Integer.toString(res10)+"\n NA");
        }

        sl6=dialog.findViewById(R.id.sl6);
        int res11 = nm+12;
        if (res11>nmmm){
            sl6.setText("");
        }else {
            sl6.setText(Integer.toString(res11)+"\n NA");
        }


        sl7=dialog.findViewById(R.id.sl7);
        int res12 = nm+13;
        if (res12>nmmm){
            sl7.setText("");
        }else {
            sl7.setText(Integer.toString(res12)+"\n NA");
        }
        thl1=dialog.findViewById(R.id.thl1);
        int res13 = nm+14;
        if (res13>nmmm){
            thl1.setText("");
        }else {
            thl1.setText(Integer.toString(res13)+"\n NA");
        }

        thl2=dialog.findViewById(R.id.thl2);
        int res14 = nm+15;
        if (res14>nmmm){
            thl2.setText("");
        }else {
            thl2.setText(Integer.toString(res14)+"\n NA");
        }

        thl3=dialog.findViewById(R.id.thl3);
        int res15 = nm+16;
        if (res15>nmmm){
            thl3.setText("");
        }else {
            thl3.setText(Integer.toString(res15)+"\n NA");
        }

        thl4=dialog.findViewById(R.id.thl4);
        int res16 = nm+17;
        if (res16>nmmm){
            thl4.setText("");
        }else {
            thl4.setText(Integer.toString(res16)+"\n NA");
        }

        thl5=dialog.findViewById(R.id.thl5);
        int res17 = nm+18;
        if (res17>nmmm){
            thl5.setText("");
        }else {thl5.setText(Integer.toString(res17)+"\n NA");}

        thl6=dialog.findViewById(R.id.thl6);
        int res18 = nm+19;
        if (res18>nmmm){
            thl6.setText("");
        }else {
            thl6.setText(Integer.toString(res18)+"\n NA");
        }

        thl7=dialog.findViewById(R.id.thl7);
        int res19 = nm+20;
        if (res19>nmmm){
            thl7.setText("");
        }else {
            thl7.setText(Integer.toString(res19)+"\n NA");
        }

        frl1=dialog.findViewById(R.id.frl1);
        int res20 = nm+21;
        if (res20>nmmm){
            frl1.setText("");
        }else {
            frl1.setText(Integer.toString(res20)+"\n NA");
        }

        frl2=dialog.findViewById(R.id.frl2);
        int res21 = nm+22;
        if (res21>nmmm){
            frl2.setText("");
        }else {
            frl2.setText(Integer.toString(res21)+"\n NA");
        }


        frl3=dialog.findViewById(R.id.frl3);
        int res22 = nm+23;
        if (res22>nmmm){
            frl3.setText("");
        }else {
            frl3.setText(Integer.toString(res22)+"\n NA");
        }

        frl4=dialog.findViewById(R.id.frl4);
        int res23 = nm+24;
        if (res23>nmmm){
            frl4.setText("");
        }else {
            frl3.setText(Integer.toString(res23)+"\n NA");
        }

        frl5=dialog.findViewById(R.id.frl5);
        int res24 = nm+25;
        if (res24>nmmm){
            frl5.setText("");
        }else {
            frl5.setText(Integer.toString(res24)+"\n NA");
        }
        frl6=dialog.findViewById(R.id.frl6);
        int res25 = nm+26;
        if (res25>nmmm){
            frl6.setText("");
        }else {
            frl6.setText(Integer.toString(res25)+"\n NA");
        }

        frl7=dialog.findViewById(R.id.frl7);
        int res26 = nm+27;
        if (res26>nmmm){
            frl7.setText("");
        }else {
            frl7.setText(Integer.toString(res26)+"\n NA");
        }


        fifl1=dialog.findViewById(R.id.fifl1);
        int res27 = nm+28;
        if (res27>nmmm){
            fifl1.setText("");
        }else {
            fifl1.setText(Integer.toString(res27)+"\n NA");
        }

        fifl2=dialog.findViewById(R.id.fifl2);
        int res28 = nm+29;
        if (res28>nmmm){
            fifl2.setText("");
        }else {
            fifl2.setText(Integer.toString(res28)+"\n NA");
        }
        fifl3=dialog.findViewById(R.id.fifl3);
        int res29 = nm+30;
        if (res29>nmmm){
            fifl3.setText("");
        }else {
            fifl3.setText(Integer.toString(res29)+"\n NA");
        }
        dialog.show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences1 = getSharedPreferences("callended", MODE_PRIVATE);
        String sss = sharedPreferences1.getString("callend", null);
        if (sss!=null){
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.aftercalllayout);
            TextView textView = dialog.findViewById(R.id.ok);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences11 = getSharedPreferences("callended",MODE_PRIVATE);
                    SharedPreferences.Editor editor11 = sharedPreferences11.edit();
                    editor11.remove("callend");
                    editor11.clear();
                    editor11.apply();

                    SharedPreferences sharedPreferences =getSharedPreferences("Callshared", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove("Clicked");
                    editor.clear();
                    editor.apply();
                    dialog.dismiss();
                }
            });
            dialog.show();
            SharedPreferences sharedPreferences11 = getSharedPreferences("callended",MODE_PRIVATE);
            SharedPreferences.Editor editor11 = sharedPreferences11.edit();
            editor11.remove("callend");
            editor11.clear();
            editor11.apply();
        }
    }
}
