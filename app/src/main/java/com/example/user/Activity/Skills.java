package com.example.user.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.HomeActivity;
import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.MainActivity;
import com.example.user.R;
import com.example.user.Response.AddFave_Repo;
import com.example.user.Response.DataList;
import com.example.user.Response.PlaceOrder_Repo;
import com.example.user.Response.RemoveFave_Repo;
import com.example.user.Response.ShortlistRepo;
import com.example.user.Response.Shortlistcount_Repo;
import com.example.user.Response.Skill_Repo_fee;
import com.example.user.Response.TagsGet_Repo;
import com.example.user.Response.TagsGet_data;
import com.example.user.adapter.Users_Fee;
import com.example.user.location.SamLocationRequestService;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.user.Activity.SearchLocation.SHARED_PREFS;

public class Skills extends MainActivity {
String name,urlImage,userid,selected,wrkid,on;
ImageView imageView,nodata,carticon;
RecyclerView recyclerView;
    private Users_Fee usersadapter;
    List<DataList> dataLists;
    private SamLocationRequestService samLocationRequestService;
    private int REQUEST_CODE=1000;
    Geocoder geocoder;
    String city,ss,locality,startdate,selecteddate;
    Integer nmmm;
    List<Address> addresses;
    Button button,sort,filter;
    ProgressBar progressBar;
    SharedPreferenceClass sharedPreferenceClass;
    JsonObject jsonObject;
    ProgressDialog progressDialog;
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private String date,today;
    FrameLayout contentFrameLayout;
    List<TagsGet_data> dataLists1;
    List<String> taglist;
    List<String> tagid;
    ArrayAdapter<String> arrayAdapter;
    Spinner spinner;
    TextView count;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_skills,contentFrameLayout);
        recyclerView=findViewById(R.id.recyclername);
        nodata=findViewById(R.id.no_data);
       //  toolbar = findViewById(R.id.toolbar);
         sort = findViewById(R.id.sort);
         filter=findViewById(R.id.filter);
        taglist=new ArrayList<>();
        tagid=new ArrayList<>();
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
       // toolbar.setNavigationIcon(R.drawable.arrowback);
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitleTextAppearance(Skills.this, R.style.PagerTabStripText2);
        setSupportActionBar(toolbar);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateandTime = sdf.format(new Date());

        SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        today = dayFormat.format(new Date());
        nmmm=  sdf.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
        SharedPreferences sharedPreferences1 = getSharedPreferences("sharedlocation", MODE_PRIVATE);
        SharedPreferences sharedPreferences2 = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String loca1 = sharedPreferences1.getString("location",null);
        String loca2 = sharedPreferences2.getString("location",null);
        if (loca2==null){
            toolbar.setTitle(loca1);
        }else {
            toolbar.setTitle(loca2);
        }

        toolbar.setSubtitle(currentDateandTime);

        carticon=toolbar.findViewById(R.id.carticon);
        count=toolbar.findViewById(R.id.count);
        progressBar= toolbar.findViewById(R.id.toolbar_progress_bar);
        progressBar.setVisibility(View.GONE);
        carticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shortlist = new Intent(Skills.this,Shortlist.class);
                startActivity(shortlist);
                finish();
            }
        });
      /*  toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Intent back = new Intent(Skills.this, HomeActivity.class);
                startActivity(back);
                finish();
            }
        });*/


        sharedPreferenceClass = new SharedPreferenceClass(Skills.this);
        userid = sharedPreferenceClass.getValue_string("user_id");
        SharedPreferences sharedPreferences = getSharedPreferences("Skillsname",MODE_PRIVATE);
        name = sharedPreferences.getString("skillname",null);
        assert urlImage != null;
        button = toolbar.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Skills.this, SearchLocation.class);
                startActivity(intent);
                finish();
            }
        });
       // progressBar= toolbar.findViewById(R.id.toolbar_progress_bar);
        //progressBar.setVisibility(View.VISIBLE);
       // button.setVisibility(View.GONE);
        progressDialog = new ProgressDialog(Skills.this);
       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Skills.this, SearchLocation.class);
                startActivity(intent);
                finish();
            }
        });*/
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent sort = new Intent(Skills.this,Sortby.class);
             startActivity(sort);
             finish();
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent filter = new Intent(Skills.this,Filterby.class);
                startActivity(filter);
                finish();
            }
        });
        geocoder = new Geocoder(this, Locale.getDefault());


        UsersDetails();
        getTags();
        getshortlistcount();

    }
    private void getshortlistcount() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Shortlistcount_Repo>call = jsonpost.getshortlistcount(data);
        call.enqueue(new Callback<Shortlistcount_Repo>() {
            @Override
            public void onResponse(Call<Shortlistcount_Repo> call, Response<Shortlistcount_Repo> response) {
                if (response.isSuccessful()){
                    Log.d("if",response.body().getMessage());
                    String counting = response.body().getData().getCount();
                    count.setText(counting);
                }else {
                }
            }

            @Override
            public void onFailure(Call<Shortlistcount_Repo> call, Throwable t) {
                Log.d("faaail",t.getMessage());
            }
        });
    }

    private void UsersDetails() {
        Map<String, String> data = new HashMap<>();
        data.put("skill",name);
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Skill_Repo_fee> call = jsonpost.getStoreData(data);
        call.enqueue(new Callback<Skill_Repo_fee>() {
            @Override
            public void onResponse(Call<Skill_Repo_fee> call, retrofit2.Response<Skill_Repo_fee> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    dataLists= response.body().getData();
                        usersadapter=new Users_Fee(Skills.this,dataLists);
                        recyclerView.setLayoutManager(new LinearLayoutManager(Skills.this, LinearLayoutManager.VERTICAL, false));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(usersadapter);
                        usersadapter.setOnITemClickListener(new Users_Fee.OnItemClickListener() {
                            @Override
                            public void onItemClick(final int position) {
                                wrkid = dataLists.get(position).getWorker_id();
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(Skills.this);
                                final View dialogView =getLayoutInflater().inflate(R.layout.placeorder_dialog, null);
                                dialog.setView(dialogView);
                                dialog.setCancelable(false);
                                Button dialog_button = (Button) dialogView.findViewById(R.id.addtag);
                                final TextView textView = dialogView.findViewById(R.id.seletedduration);
                                final Button one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,
                                        eighteen,ninteen,twenty,twentyone,twentytwo;
                                one=dialogView.findViewById(R.id.one);
                                one.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        on= one.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                two=dialogView.findViewById(R.id.two);
                                two.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= two.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                three=dialogView.findViewById(R.id.three);
                                three.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= three.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                four=dialogView.findViewById(R.id.four);
                                four.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= four.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                five=dialogView.findViewById(R.id.five);
                                five.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= five.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                six=dialogView.findViewById(R.id.six);
                                six.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= six.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                seven=dialogView.findViewById(R.id.seven);
                                seven.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= seven.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                eight=dialogView.findViewById(R.id.eight);
                                eight.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= eight.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                nine=dialogView.findViewById(R.id.nine);
                                nine.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= nine.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                ten=dialogView.findViewById(R.id.ten);
                                ten.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= ten.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                eleven=dialogView.findViewById(R.id.eleven);
                                eleven.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= eleven.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                twelve=dialogView.findViewById(R.id.twelve);
                                twelve.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= twelve.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                thirteen=dialogView.findViewById(R.id.thirteen);
                                thirteen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= thirteen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                fourteen=dialogView.findViewById(R.id.fourteen);
                                fourteen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= fourteen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                fifteen=dialogView.findViewById(R.id.fifteen);
                                fifteen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= fifteen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                sixteen=dialogView.findViewById(R.id.sixteen);
                                sixteen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= sixteen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                seventeen=dialogView.findViewById(R.id.seventeen);
                                seventeen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= seventeen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                eighteen=dialogView.findViewById(R.id.eighteen);
                                eighteen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= eighteen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                ninteen=dialogView.findViewById(R.id.ninteen);
                                ninteen.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= ninteen.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                twenty=dialogView.findViewById(R.id.twenty);
                                twenty.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= twenty.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                twentyone=dialogView.findViewById(R.id.twentyone);
                                twentyone.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= twentyone.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                twentytwo=dialogView.findViewById(R.id.twentytwo);
                                twentytwo.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         on= twentytwo.getText().toString();
                                        textView.setText("Selected "+on);
                                    }
                                });
                                String nmb = textView.getText().toString();

                                spinner = dialogView.findViewById(R.id.days);
                                spinner.setAdapter(arrayAdapter);
                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                         selected = tagid.get(i);
                                      //  Toast.makeText(Skills.this, selected, Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                                Button textView1 = dialogView.findViewById(R.id.dialog_button);
                                dialog_button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent tagintent = new Intent(Skills.this, TagsActivity.class);
                                        startActivity(tagintent);
                                    }
                                });
                                final AlertDialog alertDialog = dialog.create();
                                textView1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        SharedPreferences sharedPreferences =getSharedPreferences("SharedTag",MODE_PRIVATE);
                                       // selected = sharedPreferences.getString("tag",null);

                                        alertDialog.dismiss();
                                        progressDialog.setMessage("Wait....");
                                        progressDialog.show();
                                        placeOrder(selected,wrkid);
                                       /* if (tagv!=null){
                                        }else {

                                            Toast.makeText(Skills.this, "please add tag", Toast.LENGTH_SHORT).show();
                                        }*/


                                    }
                                });
                                alertDialog.show();
                            }

                            @Override
                            public void onShorlistItemClick(int position) {
                                progressDialog.setMessage("Wait....");
                                progressDialog.show();
                                String workerid = dataLists.get(position).getWorker_id();
                                SharedPreferences sharedPreferences =getSharedPreferences("WorkerSHARED",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("workerid",workerid);
                                editor.apply();
                                postShortlis(workerid);
                            }

                            @Override
                            public void onAddFaverateClick(int position) {
                                String workerid = dataLists.get(position).getWorker_id();
                                SharedPreferences sharedPreferences =getSharedPreferences("WorkerSHARED",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("workerid",workerid);
                                editor.apply();
                                addToFaverate(workerid);
                            }

                            @Override
                            public void onRemoveFaverateClick(int position) {
                                String workerid = dataLists.get(position).getWorker_id();
                                removeItem(workerid);
                            }

                            @Override
                            public void onCalnderpicker(int position) {
                                datePicker();


                            }

                            @Override
                            public void onavailable(int position) {
                                availabledate();
                            }

                            @Override
                            public void onrateclick(int position) {
                                final Dialog dialog = new Dialog(Skills.this);
                                dialog.setContentView(R.layout.rateme_layout);
                                RatingBar ratingBar = dialog.findViewById(R.id.ratingbarr);
                                ratingBar.setStepSize(1);
                                final TextView textView = dialog.findViewById(R.id.ratechange);
                                final ImageView imageView = dialog.findViewById(R.id.smiley);
                                Button button = dialog.findViewById(R.id.rate);
                              ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                  @Override
                                  public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                                      if (rating == 1){
                                          imageView.setImageDrawable(getResources().getDrawable(R.drawable.teri));
                                          textView.setText("Terribal");
                                      }
                                      else if (rating == 2){
                                          imageView.setImageDrawable(getResources().getDrawable(R.drawable.teribal));
                                          textView.setText("Bad");
                                      }
                                      else if (rating == 3){
                                          imageView.setImageDrawable(getResources().getDrawable(R.drawable.okay));
                                          textView.setText("Okay");
                                      }else if (rating == 4){
                                          imageView.setImageDrawable(getResources().getDrawable(R.drawable.good));
                                          textView.setText("Good");
                                      }else if (rating == 5){
                                          textView.setText("Great");
                                          imageView.setImageDrawable(getResources().getDrawable(R.drawable.great));
                                      }
                                  }
                              });
                              button.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      dialog.dismiss();
                                  }
                              });

                                dialog.show();
                            }
                        });
                        if (response.body().getData().size()==0){
                            recyclerView.setVisibility(View.GONE);
                            nodata.setVisibility(View.VISIBLE);
                        }else {
                            nodata.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                }else {
                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<Skill_Repo_fee> call, Throwable t) {
             Log.d("onfail",String.valueOf(t.getMessage()));


            }
        });
    }
    private void availabledate() {
        Dialog dialog = new Dialog(Skills.this);
        dialog.setContentView(R.layout.availabilitylayout);
        //firstlayer
        TextView fl1,fl2,fl3,fl4,fl5,fl6,fl7;
        //secondlayer
        TextView sl1,sl2,sl3,sl4,sl5,sl6,sl7;
        //thirdlayer
        TextView thl1,thl2,thl3,thl4,thl5,thl6,thl7;
        //fourthlayer
        TextView frl1,frl2,frl3,frl4,frl5,frl6,frl7;
        //fifthlayer
        TextView fifl1,fifl2,fifl3;
        fl1=dialog.findViewById(R.id.fl1);
        fl1.setText(today+"\n NA");
        fl2=dialog.findViewById(R.id.fl2);
        int nm = Integer.parseInt(today);
        int res = nm+1;
        if (res>nmmm){
            fl2.setText("");
        }else {
            fl2.setText(Integer.toString(res)+"\n NA");
        }

        fl3=dialog.findViewById(R.id.fl3);
        int res1 = nm+2;
        if (res1>nmmm){
            fl3.setText("");
        }else {
            fl3.setText(Integer.toString(res1)+"\n NA");
        }

        fl4=dialog.findViewById(R.id.fl4);
        int res2 = nm+3;
        if (res2>nmmm){
            fl4.setText("");
        }else {
            fl4.setText(Integer.toString(res2)+"\n NA");
        }

        fl5=dialog.findViewById(R.id.fl5);
        int res3 = nm+4;
        if (res3>nmmm){
            fl5.setText("");
        }else {
            fl5.setText(Integer.toString(res3)+"\n NA");
        }

        fl6=dialog.findViewById(R.id.fl6);
        int res4 = nm+5;
        if (res4>nmmm){
            fl6.setText("");
        }else {
            fl6.setText(Integer.toString(res4)+"\n NA");
        }

        fl7=dialog.findViewById(R.id.fl7);
        int res5 = nm+6;
        if (res5>nmmm){
            fl7.setText("");
        }else {
            fl7.setText(Integer.toString(res5)+"\n NA");
        }


        sl1=dialog.findViewById(R.id.sl1);
        int res6 = nm+7;
        if (res6>nmmm){
            sl1.setText("");
        }else {
            sl1.setText(Integer.toString(res6)+"\n NA");
        }

        sl2=dialog.findViewById(R.id.sl2);
        int res7 = nm+8;
        if (res7>nmmm){
            sl2.setText("");
        }else {
            sl2.setText(Integer.toString(res7)+"\n NA");
        }

        sl3=dialog.findViewById(R.id.sl3);
        int res8 = nm+9;
        if (res8>nmmm){
            sl3.setText("");
        }else {
            sl3.setText(Integer.toString(res8)+"\n NA");
        }

        sl4=dialog.findViewById(R.id.sl4);
        int res9 = nm+10;
        if (res9>nmmm){
            sl4.setText("");
        }else {
            sl4.setText(Integer.toString(res9)+"\n NA");
        }

        sl5=dialog.findViewById(R.id.sl5);
        int res10 = nm+11;
        if (res10>nmmm){
            sl5.setText("");
        }else {
            sl5.setText(Integer.toString(res10)+"\n NA");
        }

        sl6=dialog.findViewById(R.id.sl6);
        int res11 = nm+12;
        if (res11>nmmm){
            sl6.setText("");
        }else {
            sl6.setText(Integer.toString(res11)+"\n NA");
        }


        sl7=dialog.findViewById(R.id.sl7);
        int res12 = nm+13;
        if (res12>nmmm){
            sl7.setText("");
        }else {
            sl7.setText(Integer.toString(res12)+"\n NA");
        }
        thl1=dialog.findViewById(R.id.thl1);
        int res13 = nm+14;
        if (res13>nmmm){
            thl1.setText("");
        }else {
            thl1.setText(Integer.toString(res13)+"\n NA");
        }

        thl2=dialog.findViewById(R.id.thl2);
        int res14 = nm+15;
        if (res14>nmmm){
            thl2.setText("");
        }else {
            thl2.setText(Integer.toString(res14)+"\n NA");
        }

        thl3=dialog.findViewById(R.id.thl3);
        int res15 = nm+16;
        if (res15>nmmm){
            thl3.setText("");
        }else {
            thl3.setText(Integer.toString(res15)+"\n NA");
        }

        thl4=dialog.findViewById(R.id.thl4);
        int res16 = nm+17;
        if (res16>nmmm){
            thl4.setText("");
        }else {
            thl4.setText(Integer.toString(res16)+"\n NA");
        }

        thl5=dialog.findViewById(R.id.thl5);
        int res17 = nm+18;
        if (res17>nmmm){
            thl5.setText("");
        }else {thl5.setText(Integer.toString(res17)+"\n NA");}

        thl6=dialog.findViewById(R.id.thl6);
        int res18 = nm+19;
        if (res18>nmmm){
            thl6.setText("");
        }else {
            thl6.setText(Integer.toString(res18)+"\n NA");
        }

        thl7=dialog.findViewById(R.id.thl7);
        int res19 = nm+20;
        if (res19>nmmm){
            thl7.setText("");
        }else {
            thl7.setText(Integer.toString(res19)+"\n NA");
        }

        frl1=dialog.findViewById(R.id.frl1);
        int res20 = nm+21;
        if (res20>nmmm){
            frl1.setText("");
        }else {
            frl1.setText(Integer.toString(res20)+"\n NA");
        }

        frl2=dialog.findViewById(R.id.frl2);
        int res21 = nm+22;
        if (res21>nmmm){
            frl2.setText("");
        }else {
            frl2.setText(Integer.toString(res21)+"\n NA");
        }


        frl3=dialog.findViewById(R.id.frl3);
        int res22 = nm+23;
        if (res22>nmmm){
            frl3.setText("");
        }else {
            frl3.setText(Integer.toString(res22)+"\n NA");
        }

        frl4=dialog.findViewById(R.id.frl4);
        int res23 = nm+24;
        if (res23>nmmm){
            frl4.setText("");
        }else {
            frl3.setText(Integer.toString(res23)+"\n NA");
        }

        frl5=dialog.findViewById(R.id.frl5);
        int res24 = nm+25;
        if (res24>nmmm){
            frl5.setText("");
        }else {
            frl5.setText(Integer.toString(res24)+"\n NA");
        }
        frl6=dialog.findViewById(R.id.frl6);
        int res25 = nm+26;
        if (res25>nmmm){
            frl6.setText("");
        }else {
            frl6.setText(Integer.toString(res25)+"\n NA");
        }

        frl7=dialog.findViewById(R.id.frl7);
        int res26 = nm+27;
        if (res26>nmmm){
            frl7.setText("");
        }else {
            frl7.setText(Integer.toString(res26)+"\n NA");
        }


        fifl1=dialog.findViewById(R.id.fifl1);
        int res27 = nm+28;
        if (res27>nmmm){
            fifl1.setText("");
        }else {
            fifl1.setText(Integer.toString(res27)+"\n NA");
        }

        fifl2=dialog.findViewById(R.id.fifl2);
        int res28 = nm+29;
        if (res28>nmmm){
            fifl2.setText("");
        }else {
            fifl2.setText(Integer.toString(res28)+"\n NA");
        }
        fifl3=dialog.findViewById(R.id.fifl3);
        int res29 = nm+30;
        if (res29>nmmm){
            fifl3.setText("");
        }else {
            fifl3.setText(Integer.toString(res29)+"\n NA");
        }
        dialog.show();
    }

    private void getTags() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<TagsGet_Repo> call = jsonpost.gettagslist(data);
        call.enqueue(new Callback<TagsGet_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<TagsGet_Repo> call, retrofit2.Response<TagsGet_Repo> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    dataLists1 = response.body().getData();
                    for (int i=0;i<dataLists1.size();i++){
                        String tg = dataLists1.get(i).getTag();
                        String tgid = dataLists1.get(i).getId();
                        taglist.add(tg);
                        tagid.add(tgid);
                        arrayAdapter=new ArrayAdapter<String>(Skills.this, android.R.layout.simple_spinner_item, taglist);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                    }

                    if (response.body().getData().size()==0){

                    }else {

                    }

                }else {

                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<TagsGet_Repo> call, Throwable t) {
                //  Log.d("onfail",String.valueOf(t.getMessage()));

            }
        });
    }

    private void datePicker() {
        // Get Calendar instance
        final Calendar calendar = Calendar.getInstance();

        // Initialize DateSetListener of DatePickerDialog
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                // Set the selected Date Info to Calendar instance
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                // Set Date Format
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

                // Set Date in input_dob EditText
               // datepick.setText(dateFormat.format(calendar.getTime()));
                selecteddate = dateFormat.format(calendar.getTime());
               // getTimeSlots(ss);


            }
        };


        // Initialize DatePickerDialog
        DatePickerDialog datePicker = new DatePickerDialog
                (
                        this,
                        date,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
        // datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        // Show datePicker Dialog
        datePicker.show();
    }

    private void addToFaverate(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("task","add");
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<AddFave_Repo> call = jsonpost.addfaverate(jsonObject);
        call.enqueue(new Callback<AddFave_Repo>() {
            @Override
            public void onResponse(Call<AddFave_Repo> call, Response<AddFave_Repo> response) {
                if (response.isSuccessful()){
                    String s = response.body().getMessage();
                    UsersDetails();
                    usersadapter.notifyDataSetChanged();
                    Toast.makeText(Skills.this,s, Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(Skills.this, "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddFave_Repo> call, Throwable t) {

            }
        });
    }

    private void removeItem(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("task","remove");
        Toast.makeText(Skills.this, workerid+" "+userid, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<RemoveFave_Repo> call = jsonpost.removefav(jsonObject);
        call.enqueue(new Callback<RemoveFave_Repo>() {
            @Override
            public void onResponse(Call<RemoveFave_Repo> call, Response<RemoveFave_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    String msg = response.body().getMessage();
                    UsersDetails();
                    usersadapter.notifyDataSetChanged();
                     Toast.makeText(Skills.this, msg, Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(Skills.this,"else  "+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RemoveFave_Repo> call, Throwable t) {
                Toast.makeText(Skills.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void placeOrder(String tagv,String wrkid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("worker_id",wrkid);
        jsonObject.addProperty("tag_id",tagv);
        jsonObject.addProperty("no_of_days",on);
        jsonObject.addProperty("order_start_date",selecteddate);
        Toast.makeText(this, tagv+" no_of_days:"+on, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<PlaceOrder_Repo> call = jsonpost.placeorder(jsonObject);
        call.enqueue(new Callback<PlaceOrder_Repo>() {
            @Override
            public void onResponse(Call<PlaceOrder_Repo> call, Response<PlaceOrder_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    assert response.body() != null;
                    String s = response.body().getMessage();
                    Toast.makeText(Skills.this, s, Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPreferences = getSharedPreferences("SharedTag",MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove("tag");
                    editor.apply();
                    Intent success = new Intent(Skills.this,SuccessFull.class);
                    startActivity(success);
                    finish();

                }else {
                    progressDialog.dismiss();
                    Toast.makeText(Skills.this, "else", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlaceOrder_Repo> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("ff",t.getMessage());
                Toast.makeText(Skills.this, "onfail "+t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void postShortlis(String workerid) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("worker_id",workerid);
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("task","add");
       // Toast.makeText(Skills.this, userid, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<ShortlistRepo> call = jsonpost.postshort(jsonObject);
        call.enqueue(new Callback<ShortlistRepo>() {
            @Override
            public void onResponse(Call<ShortlistRepo> call, Response<ShortlistRepo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    assert response.body() != null;
                    String s = response.body().getMessage();
                    Toast.makeText(Skills.this,s, Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(Skills.this, "else", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShortlistRepo> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Skills.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences1 = getSharedPreferences("callended", MODE_PRIVATE);
        String sss = sharedPreferences1.getString("callend", null);
        if (sss!=null){
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.aftercalllayout);
            TextView textView = dialog.findViewById(R.id.ok);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences11 = getSharedPreferences("callended",MODE_PRIVATE);
                    SharedPreferences.Editor editor11 = sharedPreferences11.edit();
                    editor11.remove("callend");
                    editor11.clear();
                    editor11.apply();

                    SharedPreferences sharedPreferences =getSharedPreferences("Callshared", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove("Clicked");
                    editor.clear();
                    editor.apply();
                    dialog.dismiss();
                }
            });
            dialog.show();
            SharedPreferences sharedPreferences11 = getSharedPreferences("callended",MODE_PRIVATE);
            SharedPreferences.Editor editor11 = sharedPreferences11.edit();
            editor11.remove("callend");
            editor11.clear();
            editor11.apply();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE){
            samLocationRequestService.startLocationUpdates();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent back = new Intent(Skills.this, HomeActivity.class);
        startActivity(back);
        finish();
    }

}
