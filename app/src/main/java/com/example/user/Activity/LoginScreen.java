package com.example.user.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.HomeActivity;
import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.R;
import com.example.user.Response.Login_Data;
import com.example.user.Response.Login_res;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.example.user.utils.Server_Links;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginScreen extends AppCompatActivity {
    Button button;
    // ProgressBar progressBar;
    Handler handler = new Handler();
    Runnable runnable;
    Timer timer;

    TextView textView;
    EditText et1, et2;

    private InternetConnection internetConnection;
    private SharedPreferenceClass sharedPreferenceClass;
    private SharedPreferences sharedPreferences;
    public static final String USER_PREFS = "SeekingDaddie";
    public static final String KEY_PHONE = "phone_no";
    public static final String KEY_PASSWORD = "password";
    private CircularProgressBar progressBar;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        button=findViewById(R.id.btn_signUp);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        internetConnection = new InternetConnection(this);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        progressBar = findViewById(R.id.loginprogress);
        button = findViewById(R.id.btn_signUp);
        textView = findViewById(R.id.textView);
        et1 = findViewById(R.id.etphn);
        et2 = findViewById(R.id.etpass);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                validate();
            }
        });
    }

    private void validate() {
        String phonenew=et1.getText().toString();
        String passnew=et2.getText().toString();
        //  final   String phno=sharedPreferences.getString("phone", "");

        if (phonenew.equalsIgnoreCase("") || passnew.equalsIgnoreCase(""))
        {

            if(phonenew.equalsIgnoreCase( "")){
                progressBar.setVisibility(View.GONE);
                Snackbar.make(findViewById(android.R.id.content), "Please enter registered phone number", Snackbar.LENGTH_LONG).show();
                et1.requestFocus();
                et1.setCursorVisible(true);
            }
            else if(passnew.equalsIgnoreCase( "")){
                progressBar.setVisibility(View.GONE);
                Snackbar.make(findViewById(android.R.id.content), "Please enter valid password", Snackbar.LENGTH_LONG).show();
                et2.requestFocus();
                et2.setCursorVisible(true);
            }

        }
        else {
            if (internetConnection.isConnectingToInternet()) {
               //login();
                Login();
            } else {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }

    }

    private void Login() {
        String ed1= et1.getText().toString();
        String ed2=et2.getText().toString();
        Toast.makeText(this, ed1, Toast.LENGTH_SHORT).show();
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("phone_no",ed1);
        hashMap.put("password",ed2);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<Login_res> call = jsonpost.login(hashMap);
        call.enqueue(new Callback<Login_res>() {
            @Override
            public void onResponse(Call<Login_res> call, retrofit2.Response<Login_res> response) {
                if (response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginScreen.this, "success", Toast.LENGTH_SHORT).show();
                    Login_Data login_data = response.body().getData();
                    String user_id = login_data.getId();
                    String name = login_data.getFirst_name();
                    Toast.makeText(LoginScreen.this, "userid"+user_id, Toast.LENGTH_SHORT).show();
                    sharedPreferenceClass.setValue_string("user_id",user_id);
                    sharedPreferenceClass.setValue_string("fname",name);
                    Intent intent = new Intent(LoginScreen.this, HomeActivity.class);
                    startActivity(intent);
                    finish();

                }else{
                    Snackbar.make(findViewById(android.R.id.content), "Invalid Login ", Snackbar.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Login_res> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(findViewById(android.R.id.content), "Try again!! ", Snackbar.LENGTH_LONG).show();

            }
        });

    }




    public void signup(View view) {
        Intent btnsignup = new Intent(LoginScreen.this, SignUp.class);
        startActivity(btnsignup);
        finish();
        handler = new Handler();
        runnable = new Runnable()
        {
            @Override
            public void run()
            {
                progressBar.setVisibility(View.GONE);
                timer.cancel();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                handler.post(runnable);
            }
        },10000);

    }

    public void forgotpassword(View view) {
        Intent intent=new Intent(LoginScreen.this, ForgotpasswordActivity.class);
        startActivity(intent);
        finish();
    }
}
