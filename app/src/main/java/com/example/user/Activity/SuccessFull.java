package com.example.user.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.HomeActivity;
import com.example.user.MainActivity;
import com.example.user.R;

public class SuccessFull extends AppCompatActivity {
TextView txt,ttt;
Button track;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_full);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        Typeface font = Typeface.createFromAsset(getAssets(),"font/Uber Move Text.ttf");
        txt=findViewById(R.id.txt);
        ttt=findViewById(R.id.ttt);
        track=findViewById(R.id.track);
        txt.setTypeface(font);
        ttt.setTypeface(font);
        track.setTypeface(font);
        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SuccessFull.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SuccessFull.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
