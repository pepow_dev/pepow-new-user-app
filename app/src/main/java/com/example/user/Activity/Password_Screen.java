package com.example.user.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.HomeActivity;
import com.example.user.R;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.example.user.utils.Server_Links;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Password_Screen extends AppCompatActivity {
Button btn;
    EditText et_pwd;
    EditText et_repwd;
    Button btnsubmit;
    String os_ver,device_name,device_id,entry_date,model,screen_size;
    private  String user_id;
    private SharedPreferences sharedPreferences;
    private SharedPreferenceClass sharedPreferenceClass;

    public static  String KEY_FIRST = "first_name";
    public static  String KEY_lAST = "last_name";
    public static String KEY_PHONENO="phone_no";

    public static final String KEY_SUBMITOTP = "submit_otp";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_CONFIRMPASSWORD = "confirm_password";
    private InternetConnection internetConnection;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password__screen);
        btn=findViewById(R.id.btnsubmit);
        et_pwd = findViewById(R.id.etpassword);
        et_repwd = findViewById(R.id.etconfirmpassword);
        internetConnection = new InternetConnection(this);
        sharedPreferenceClass=new SharedPreferenceClass(this);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pwdvalidation();
            }
        });
    }

    private void pwdvalidation() {
        String passwordInput = et_pwd.getText().toString().trim();
        final String password = et_pwd.getText().toString();
        final String confirmpassword = et_repwd.getText().toString();


        if (et_pwd.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Field can't be empty", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);


        } else if (et_pwd.toString().length() <= 7) {
            Snackbar.make(findViewById(android.R.id.content), "Minimum 8 characters", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);

        } else if (et_repwd.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Re-enter your password ", Snackbar.LENGTH_LONG).show();

            et_repwd.requestFocus();
            et_repwd.setCursorVisible(true);
        } else if ((et_pwd.getText().toString().equals("")) || et_repwd.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Both passwords are not matching", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);
        }
        else if ((et_repwd.length() <= 7)) {
            Snackbar.make(findViewById(android.R.id.content), "Minimum 8 characters", Snackbar.LENGTH_LONG).show();
            et_repwd.requestFocus();
            et_repwd.setCursorVisible(true);

        } else if ((et_pwd.length() <= 7) || et_repwd.length() <= 7) {
            Snackbar.make(findViewById(android.R.id.content), "Minimum 8 characters", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);


        } else if (passwordInput.isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content),
                    "Field can't be empty", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);


        } else if (!confirmpassword.equals(password)) {
            Snackbar.make(findViewById(android.R.id.content), "Both passwords are not matching", Snackbar.LENGTH_LONG).show();
        } else {

            if (internetConnection.isConnectingToInternet()) {
                submit();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }

    }
    public String getDeviceScreenResolution()
    {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x; //device width
        int height = size.y; //device height

        return "" + width + " x " + height; //example "480 * 800"
    }

    private void getDeviceInformation()
    {
        device_id = Settings.Secure.getString(Password_Screen.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        device_name = Build.PRODUCT;
        model = Build.MODEL;
        os_ver = Build.VERSION.RELEASE;
        screen_size = getDeviceScreenResolution();


    }

    @Override
    protected void onResume()
    {
        super.onResume();

        getDeviceInformation();
    }
    private void submit() {
        sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
           final   String fname=sharedPreferences.getString("fname", null);
         final  String lname=sharedPreferences.getString("lname", null);
        final   String phno=sharedPreferences.getString("phone", null);
        final  String otp=sharedPreferences.getString("submitotp", null);
        final String password = et_pwd.getText().toString();
        final String confirmpassword = et_repwd.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Server_Links.WORKER_REGD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");

                    Toast.makeText(getApplicationContext(),status + " "  +message + "  ",Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("Success")) {

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");

                        String  ph_no=jsonObject1.getString("phone_no");
                        sharedPreferenceClass.setValue_string("user_id",user_id);

                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.putString("phone","phone_no");
                        editor.apply();

                        sharedPreferenceClass.setPhone_num(ph_no);
                        //Show popup messages
                        final Dialog dialog = new Dialog(Password_Screen.this);
                        dialog.setCancelable(false);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.success_dialog);
                        Button ok = (Button) dialog.findViewById(R.id.ok);

                        dialog.setCancelable(false);

                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent btnintent = new Intent(Password_Screen.this, HomeActivity.class);
                                startActivity(btnintent);
                                finish();
                                dialog.dismiss();

                            }
                        });

                        dialog.show();

                    }
                    else {

                        Intent intent=new Intent(Password_Screen.this,Rejected_Activity.class);
                        startActivity(intent);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();

                params.put(KEY_FIRST, fname);
                params.put(KEY_lAST,lname);
                params.put("device_id", device_id);
                params.put("device_name",device_name);
                params.put("os_ver",os_ver);
                params.put("model",model);
                params.put("screen_size",screen_size);
                params.put("modify_date","  ");
                params.put(KEY_PASSWORD, password);
                params.put(KEY_PHONENO, phno);
                params.put(KEY_SUBMITOTP, otp);
                params.put(KEY_CONFIRMPASSWORD, confirmpassword);


                return params;

            }
        };
        stringRequest.setShouldCache(false);
        int socketTimeout = 60000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(Password_Screen.this);
        requestQueue.add(stringRequest);
        //successregistration();


    }
}
