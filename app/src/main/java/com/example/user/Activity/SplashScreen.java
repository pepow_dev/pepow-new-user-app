package com.example.user.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.user.HomeActivity;
import com.example.user.MainActivity;
import com.example.user.R;
import com.example.user.sharedpreference.SharedPreferenceClass;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 7000;
    ImageView img;
    private SharedPreferenceClass sharedPreferenceClass;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        img=findViewById(R.id.image);
      sharedPreferenceClass = new SharedPreferenceClass(SplashScreen.this);
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.myanimation);
        img.setAnimation(anim);
        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
/*Intent intent=new Intent(SplashScreen.this, LoginScreen.class);
startActivity(intent);
finish();*/

               String user_id = sharedPreferenceClass.getValue_string("user_id");
                if (!user_id.equals("")) {
                    Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }

                else {
                    Intent i = new Intent(SplashScreen.this, LoginScreen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }


            }
        },SPLASH_TIME_OUT);
    }
}
