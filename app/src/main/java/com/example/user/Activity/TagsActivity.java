package com.example.user.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.user.Interface.RetrofitInterface;
import com.example.user.Interface.ServiceGenerator;
import com.example.user.MainActivity;
import com.example.user.R;
import com.example.user.Response.ShortgetList_Data;
import com.example.user.Response.ShortgetList_Repo;
import com.example.user.Response.ShortlistRepo;
import com.example.user.Response.TagPost_Repo;
import com.example.user.Response.TagPost_data;
import com.example.user.Response.TagsGet_Repo;
import com.example.user.Response.TagsGet_data;
import com.example.user.adapter.GetTagsAdapter;
import com.example.user.adapter.Shortlist_Adapter;
import com.example.user.sharedpreference.SharedPreferenceClass;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagsActivity extends AppCompatActivity  {
    Toolbar toolbar;
   EditText tag;
   Button addtag;
    SharedPreferenceClass sharedPreferenceClass;
    JsonObject jsonObject;
    String userid,tagvalue;
    GridLayoutManager manager;
    RecyclerView tagrecycler;
     GetTagsAdapter tags_adapter;
    List<TagsGet_data> dataLists;
    ImageView refresh;
    String tagid;
    ProgressDialog progressDialog;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);
        toolbar=findViewById(R.id.tagtoolbar);
        refresh=findViewById(R.id.refresh);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        toolbar.setNavigationIcon(R.drawable.arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tag=findViewById(R.id.tags);
        addtag=findViewById(R.id.addtags);
        sharedPreferenceClass = new SharedPreferenceClass(TagsActivity.this);
         userid = sharedPreferenceClass.getValue_string("user_id");
        progressDialog=new ProgressDialog(TagsActivity.this);
         tagrecycler=findViewById(R.id.tagrecycle);
        manager = new GridLayoutManager(TagsActivity.this, 3, GridLayoutManager.VERTICAL, false);
        tagrecycler.setLayoutManager(manager);
        tagrecycler.setHasFixedSize(true);

         addtag.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 progressDialog.setMessage("Wait....");
                 progressDialog.show();
                 tagvalue = tag.getText().toString();
                 SharedPreferences sharedPreferences = getSharedPreferences("SharedTag",MODE_PRIVATE);
                 SharedPreferences.Editor editor = sharedPreferences.edit();
                 editor.putString("tag",tagid);
                 editor.apply();
                 postTag(tagvalue);
                onBackPressed();

             }
         });
         refresh.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 refresh.clearAnimation();
                 RotateAnimation anim = new RotateAnimation(30, 360, refresh.getWidth()/2, refresh.getHeight()/2);
                 anim.setFillAfter(true);
                 anim.setRepeatCount(0);
                 anim.setDuration(800);
                 refresh.startAnimation(anim);
                 getTags();
             }
         });

getTags();
    }

    private void postTag(String tagvalue) {
        jsonObject=new JsonObject();
        jsonObject.addProperty("user_id",userid);
        jsonObject.addProperty("tag",tagvalue);
        Toast.makeText(TagsActivity.this, userid+" "+tagvalue, Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<TagPost_Repo> call = jsonpost.posttag(jsonObject);
        call.enqueue(new Callback<TagPost_Repo>() {
            @Override
            public void onResponse(Call<TagPost_Repo> call, Response<TagPost_Repo> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    assert response.body() != null;
                    String s = response.body().getMessage();
                     tagid = response.body().getData().getTag_id();
                    SharedPreferences sharedPreferences = getSharedPreferences("SharedTag",MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("tag",tagid);
                    editor.apply();
                    onBackPressed();
                    Toast.makeText(TagsActivity.this, s, Toast.LENGTH_SHORT).show();


                }else {
                    progressDialog.dismiss();
                    Toast.makeText(TagsActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TagPost_Repo> call, Throwable t) {
                Log.d("onfail",t.getMessage());
                progressDialog.dismiss();
                //Toast.makeText(TagsActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getTags() {
        Map<String, String> data = new HashMap<>();
        data.put("user_id",userid);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://ujalvomigroup.com/");
        Call<TagsGet_Repo> call = jsonpost.gettagslist(data);
        call.enqueue(new Callback<TagsGet_Repo>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onResponse(Call<TagsGet_Repo> call, retrofit2.Response<TagsGet_Repo> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    dataLists = response.body().getData();
                    tags_adapter=new GetTagsAdapter(TagsActivity.this,dataLists);
                    tagrecycler.setHasFixedSize(true);
                    tagrecycler.setAdapter(tags_adapter);
                       tags_adapter.setOnITemClickListener(new GetTagsAdapter.OnItemClickListener() {
                           @Override
                           public void onItemClick(int position) {
                               String ss = dataLists.get(position).getTag();
                               tag.setText(ss);
                               tagid = dataLists.get(position).getId();
                               Toast.makeText(TagsActivity.this, tagid, Toast.LENGTH_SHORT).show();
                               SharedPreferences sharedPreferences = getSharedPreferences("SharedTag",MODE_PRIVATE);
                               SharedPreferences.Editor editor = sharedPreferences.edit();
                               editor.putString("tag",tagid);
                               editor.apply();
                             //  postTag(ss);

                           }
                       });
                    if (response.body().getData().size()==0){
                        tagrecycler.setVisibility(View.GONE);

                        // nodata.setVisibility(View.VISIBLE);
                    }else {
                        // nodata.setVisibility(View.GONE);
                        tagrecycler.setVisibility(View.VISIBLE);
                    }

                }else {

                    Log.d("else", String.valueOf(response.errorBody()));
                }
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onFailure(Call<TagsGet_Repo> call, Throwable t) {
              //  Log.d("onfail",String.valueOf(t.getMessage()));

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


}
