package com.example.user.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.user.R;
import com.google.android.material.snackbar.Snackbar;

public class SignUp extends AppCompatActivity {
    Button button;
    EditText et_fname, et_lname;
    private static final String TAG = "RegdOneActivity";
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.themecolourblue));
        button=findViewById(R.id.next);
        et_fname = findViewById(R.id.etfirstname);
        et_lname = findViewById(R.id.etlastname);
        internetConnection = new InternetConnection(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
                sharedPreferences = getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("fname", et_fname.getText().toString());
                editor.putString("lname", et_lname.getText().toString());
                editor.apply();
            }
        });
    }
    private void validation() {


        if (et_fname.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Please enter all the fields ", Snackbar.LENGTH_LONG).show();
            et_fname.requestFocus();
            et_fname.setCursorVisible(true);


        } else if (et_fname.toString().length() <= 2) {
            Snackbar.make(findViewById(android.R.id.content), " First name should be minimum 3 letters ", Snackbar.LENGTH_LONG).show();
            et_fname.requestFocus();
            et_fname.setCursorVisible(true);

        } else if (et_lname.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Please enter  your last  name ", Snackbar.LENGTH_LONG).show();

            et_lname.requestFocus();
            et_lname.setCursorVisible(true);
        } else if ((et_lname.length() <= 2)) {
            Snackbar.make(findViewById(android.R.id.content), " Last name should be minimum 3 letters ", Snackbar.LENGTH_LONG).show();
            et_lname.requestFocus();
            et_lname.setCursorVisible(true);
        } else if ((et_fname.length() <= 2) || et_lname.length() <= 2) {
            Snackbar.make(findViewById(android.R.id.content), " First name should be minimum 3 letters", Snackbar.LENGTH_LONG).show();
            et_fname.requestFocus();
            et_fname.setCursorVisible(true);
        }



        else {

            if (internetConnection.isConnectingToInternet()) {
                Intent intent = new Intent(SignUp.this, RegisterPhone.class);
                startActivity(intent);
                  finish();

            } else {
                Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }

    }
}
