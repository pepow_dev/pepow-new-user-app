package com.example.user.model;

public class Fav_Model {
    String name, skill, area, phone, visitcharge, price,lname,image,secondskill,userid;
    float distance;


    public Fav_Model(String name, String skill, String area, String phone, String visitcharge, String price, String lname, String image, String secondskill, String id, float distance) {
        this.name = name;
        this.skill = skill;
        this.area = area;
        this.phone = phone;
        this.visitcharge = visitcharge;
        this.price = price;
        this.lname = lname;
        this.image = image;
        this.secondskill = secondskill;
        this.userid = id;
        this.distance=distance;

    }


    public String getId() {
        return userid;
    }

    public void setId(String id) {
        this.userid = id;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSecondskill() {
        return secondskill;
    }

    public void setSecondskill(String secondskill) {
        this.secondskill = secondskill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVisitcharge() {
        return visitcharge;
    }

    public void setVisitcharge(String visitcharge) {
        this.visitcharge = visitcharge;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
