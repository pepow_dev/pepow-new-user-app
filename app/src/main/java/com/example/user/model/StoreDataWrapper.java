package com.example.user.model;

import java.util.List;

public class StoreDataWrapper {
    private List<StoreData> data;

    public List<StoreData> getCoupons() {
        return data;
    }

    public void setCoupons(List<StoreData> coupons) {
        this.data = coupons;
    }
}
